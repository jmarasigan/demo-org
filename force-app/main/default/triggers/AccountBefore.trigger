trigger AccountBefore on Account (before insert) {

    
    for(Account AccRec: Trigger.new){
        if(AccRec.ShippingStreet==null &&
           AccRec.ShippingCity==null &&
           AccRec.ShippingPostalCode==null &&
           AccRec.ShippingState==null &&
           AccRec.ShippingCountry==null){
               
           AccRec.ShippingStreet = AccRec.BillingStreet;
           AccRec.ShippingPostalCode = AccRec.BillingPostalCode;
           AccRec.ShippingState = AccRec.BillingState;
           AccRec.ShippingCity = AccRec.BillingCity;
           AccRec.ShippingCountry = AccRec.BillingCountry;                
           }

        
        if(AccRec.BillingCountry == 'USA' ||
           AccRec.BillingCountry == 'United States'  ||
           AccRec.BillingCountry == 'United States of America'){
                
                AccRec.BillingCountry ='US';
              }
        
        if(AccRec.ShippingCountry == 'USA' ||
           AccRec.ShippingCountry == 'United States' ||
           AccRec.ShippingCountry == 'United States of America'){
               
            AccRec.ShippingCountry ='US';
        }
        
        
        if(AccRec.BillingCountry =='US'){
            if( AccRec.Phone !=null){

                if(AccRec.Phone.left(2) != '+1'){
                
                     AccRec.addError('US telephone numbers must start with +1');
                     }
              }
            if( AccRec.Fax !=null){

                 if(AccRec.fax.left(2) != '+1'){
                
                      AccRec.addError('US telephone numbers must start with +1');
                     }

                 }
            }
        }
}