trigger AccountTrigger on Account (before insert, after update) 
{
    
   if(Trigger.isBefore && Trigger.isInsert)
    {
        AccountTriggerHandler.doBeforeInsert(Trigger.new);
    }
    else if(Trigger.isAfter && Trigger.isUpdate)
    {
        AccountTriggerHandler.doBeforeUpdate(Trigger.new, Trigger.oldMap, Trigger.newMap);    
    }
    
}