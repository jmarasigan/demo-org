@isTest
public class ContactTriggerHandler_Test {
    //FROM force.com IDE
    @isTest static void ContactNegativeInsert(){
    
    insert SetUpData_Test.getPositiveSingleAccount();
    insert SetUpData_Test.getSingleContact();
 
        
        Test.startTest();
        try{
            
     	insert SetUpData_Test.getNegativeSingleContact();

        Test.stopTest();
            }
        
            catch(DmlException e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Invalid Contact. An Account may only have 1 Primary Contact.');
            system.assertEquals(expectedExceptionThrown,true);
            }
    }

    @isTest static void ContactPositiveInsert(){
    
        
        Test.startTest();
           insert SetUpData_Test.getPositiveSingleAccount();
           insert SetUpData_Test.getSingleContact();
    	   insert SetUpData_Test.getPositiveSingleContact();
       
        Test.stopTest();
        
        List<Contact> cont =[SELECT Primary__c FROM Contact WHERE LastName='Mara'];
        for(Contact cont2: cont){
        system.assertEquals(cont2.Primary__c, FALSE);
        }
    }
    
    @isTest static void ContactNegativeUpdate(){
         
        insert SetUpData_Test.getPositiveSingleAccount();
        insert SetUpData_Test.getSingleContact();
        insert SetUpData_Test.getPositiveSingleContact();
        
        
        Contact con = [SELECT Primary__c FROM Contact WHERE (LastName='Mara')];
        con.Primary__c=TRUE;
        
         Test.startTest();
                 try{
                 update con;

         Test.stopTest();
                    }
             catch(DmlException e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Invalid Contact. An Account may only have 1 Primary Contact.');
            system.assertEquals(expectedExceptionThrown,true);
            }
     }
    
    @isTest static void BulkPositiveContact(){
             
             insert SetUpData_Test.getPositiveSingleAccount();
             insert SetUpData_Test.getSingleContact();
             
         Test.startTest();
             try{
                 insert SetUpData_Test.getBulkContact();

         Test.stopTest();
                }
             catch(DmlException e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Invalid Contact. An Account may only have 1 Primary Contact.');
            system.assertEquals(expectedExceptionThrown,true);
            }
     }
    
    @isTest static void BulkNegativeContact(){
             
             insert SetUpData_Test.getPositiveSingleAccount();
             insert SetUpData_Test.getSingleContact();
             
         Test.startTest();
             try{
                 insert SetUpData_Test.getNegativeBulkContact();
    
         Test.stopTest();
                }
             catch(DmlException e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Invalid Contact. An Account may only have 1 Primary Contact.');
            system.assertEquals(expectedExceptionThrown,true);
            }
     }

 
 
}