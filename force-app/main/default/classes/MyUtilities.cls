public class MyUtilities {

    //instance variable
   
    private String instanceVar;
    
    //class variable
    private static String classLevelVar;
    
    //instance method 
    
    public void setInstanceVar(String newValue){
        
        instanceVar =newValue;
       
    }  
    
        public String getInstanceVar(){
        
        return instanceVar;
    }
    
    //class method
    
    
    public static void setClassLevelVar(String newValue){
        
        classLevelVar = newValue;
    }
    
        public static String getClassLevelVar(){
        
        return classLevelVar;
    }

          
}