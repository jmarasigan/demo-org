@isTest
global class SampleRestCalloutMock_Test implements HttpCallOutMock {
	
    
     global HTTPResponse respond(HTTPRequest req) {
         
        HttpResponse res;
        
        if(req.getEndpoint() == 'https://maps.googleapis.com/maps/api/geocode/json?address=139+Valero,+Makati,+1227+Kalakhang+Maynila&sensor=false'){
        res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');

        res.setBody('json');

        res.setStatusCode(200);

        return res;
        
        //res = getjson();
        }
        else{
        res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');

        res.setBody('xml');

        res.setStatusCode(200);

        return res;
        
        //res = getxml();
        }
    }
    
      /*  public static HttpResponse getjson() {
 

       	HttpResponse res = new HttpResponse();
        
        res.setHeader('Content-Type', 'application/json');

        res.setBody('json');
        
        res.setStatusCode(200);
        
        return res;

}

      public static HttpResponse getxml() {
 

        HttpResponse res = new HttpResponse();
        
        res.setHeader('Content-Type', 'application/xml');

        res.setBody('xml');
        
        res.setStatusCode(200);
        
        return res;

}*/
  


}