global class UniversalOnlineShop {

	global class Productdetails{
		
		webService string prodId;
    	webService string name;
    	webService string cat;
    	webService string code;
		webService decimal price;
    	webService decimal stock;
	}
	
	   global class OrderResponseDetails{
        
        webService string orderNumber;
        webService string message;  
        webService string status;
        
    }
    
    global class AccountServiceDetails{
        webService string AccountId;
    }
    
    global class inputOrderRequestDetails{
        webService string productCode2;
        webService decimal quantity;
        
    }
    
    webService static List<Productdetails> getProductItems(){
    	
    	List <Productdetails> pro = new List <Productdetails>();

    	String queryStr = 'SELECT Id, Name, Family, ProductCode, Quantity_In_Stock__c FROM Product2';
		
		List <Product2> prod2 = Database.query(queryStr);
		
		String queryStr2 ='SELECT Id,UnitPrice,Product2Id,Pricebook2Id FROM PricebookEntry WHERE Product2Id IN:prod2' ;
		
		List<PricebookEntry> priceEntry = Database.query(queryStr2);
		
		    Map<Id,PricebookEntry> price = new Map<Id,PricebookEntry>();
        
        for(PricebookEntry priceRec: priceEntry){
            
            price.put(priceRec.Product2Id,priceRec);
        } 
				
        for(Product2 prodRec:prod2){
        	
            Productdetails prodItem = new Productdetails();
            
            prodItem.prodId = prodRec.Id;
            prodItem.name = prodRec.Name;
            prodItem.cat = prodRec.Family;            
            prodItem.code = prodRec.ProductCode;
            prodItem.stock = prodRec.Quantity_In_Stock__c;
            
        if(price.containsKey(prodRec.Id)){
                
           prodItem.price =price.get(prodRec.Id).UnitPrice; 
        }
            
            pro.add(prodItem);
        }
    
    	return pro;
    	

    }
    
    webService static List<OrderResponseDetails> getsubmitOrder(string accId, List<inputOrderRequestDetails> ordr){
    	
    	
    	List<OrderResponseDetails> message = new List<OrderResponseDetails>();
    	
    	String queryStr ='SELECT Id, AccountId FROM Contract WHERE Status =\'Activated\' AND AccountId =:accId LIMIT 1';
    	
    	List<Contract> cont =  Database.query(queryStr);
    	
    			if(cont.isEmpty()){
					
					OrderResponseDetails reply = new OrderResponseDetails();
					
					    reply.orderNumber = '';
                        reply.message ='Invalid Account Specified';
                        reply.status ='Failed';               
                        message.add(reply);
				}else{
				
				
				
        Contract con = cont.get(0);
        
            Order ord = new Order();
            
            ord.contractId =con.Id;
            ord.AccountId = con.AccountId;
            ord.EffectiveDate =System.today()+10;
            ord.Status ='Draft';
                        
            insert ord;
            
            String queryPriceBookEntry ='SELECT ID,ProductCode,UnitPrice FROM PricebookEntry';
            
            List<PriceBookEntry> price =Database.query(queryPriceBookEntry);
            
            Map<String,PricebookEntry> pricemap = new  Map<String,PricebookEntry>();
            

                         
             for(PricebookEntry priceb: price){
                
                pricemap.put(priceb.ProductCode,priceb);
             }
             
     
             
              List<OrderItem> ordItem = new List<OrderItem>();
             
                    for( inputOrderRequestDetails inputOR: ordr){
                
              
                     OrderItem orItem = new OrderItem();
                     orItem.OrderId = ord.Id;  
					 orItem.quantity = inputOR.Quantity; 

                     
                     if(pricemap.containsKey(inputOR.productCode2)){
                        
                 		String quan = String.valueOf(orItem.quantity);
                    
                    		if(orItem.quantity < 0 || (!quan.contains('.00') && quan.contains('.')) ){
                           		OrderResponseDetails reply = new OrderResponseDetails();
              
                          		 reply.orderNumber = '';    
                          		 reply.message ='Invalid Quantity Specified';
                           		 reply.status ='Failed';              
                           		 message.add(reply);
                         	} else{
                         		orItem.PriceBookEntryId =pricemap.get(inputOR.productCode2).Id;
                    			orItem.unitPrice = pricemap.get(inputOR.productCode2).UnitPrice; 
                         		                    
                     			ordItem.add(orItem);
                     			 
                         	}
                    } 
                    	else{
                    	OrderResponseDetails reply = new OrderResponseDetails();
					
					    reply.orderNumber = '';
                        reply.message ='Invalid Product Code Specified';
                        reply.status ='Failed';               
                        message.add(reply);

                    }
    
                 
                    }
                    
                   insert ordItem;
        			
        			
        			ord.Status='Activated';
       				update ord;
       				
                    if(!ordItem.isEmpty()){
                    	for(OrderItem orderedItem :ordItem){
           
                        OrderResponseDetails reply = new OrderResponseDetails();
              
                        reply.orderNumber = ord.Id;
                        reply.message ='Order saved sucessfully';
                        reply.status ='Success';               
                        message.add(reply);
                        

           }    
        }

      }	

            return message; 
				   
    }	

}