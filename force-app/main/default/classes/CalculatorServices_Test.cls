@isTest
public class CalculatorServices_Test {

    @isTest static void doAdd(){
        
        Test.setMock(WebServiceMock.class, new CalculatorServicesMock_Test(CalculatorServicesMock_Test.Operations.Add));
        
        Test.startTest();
        CalculatorServices.CalculatorImplPort calc= new CalculatorServices.CalculatorImplPort();
		Double addResult = calc.doAdd(7,1);
        Test.stopTest();
        
        system.assertEquals(8.0 , addResult);
    }
    
        @isTest static void doSubtract(){
        
        Test.setMock(WebServiceMock.class, new CalculatorServicesMock_Test(CalculatorServicesMock_Test.Operations.Subtract));
        
        Test.startTest();
        CalculatorServices.CalculatorImplPort calc= new CalculatorServices.CalculatorImplPort();
		Double addResult = calc.doSubtract(7,1);
        Test.stopTest();
        
        system.assertEquals(6.0 , addResult);
    }
            @isTest static void doDivide(){
        
        Test.setMock(WebServiceMock.class, new CalculatorServicesMock_Test(CalculatorServicesMock_Test.Operations.Divide));
        
        Test.startTest();
        CalculatorServices.CalculatorImplPort calc= new CalculatorServices.CalculatorImplPort();
		Double addResult = calc.doDivide(7,7);
        Test.stopTest();
        
        system.assertEquals(1.0 , addResult);
    }
    
                @isTest static void doMultiply(){
        
        Test.setMock(WebServiceMock.class, new CalculatorServicesMock_Test(CalculatorServicesMock_Test.Operations.Multiply));
        
        Test.startTest();
        CalculatorServices.CalculatorImplPort calc= new CalculatorServices.CalculatorImplPort();
		Double addResult = calc.doMultiply(7,7);
        Test.stopTest();
        
        system.assertEquals(49.0 , addResult);
    }
    
}