public class trialController {
    
    
    @AuraEnabled
    public static Id saveTheFile(String base64Data, Id contentDocumentId, Id parentId,String recId, String filename){
          
        
        String base64Data2 = base64Data; //EncodingUtil.urlDecode(base64Data,'UTF-8');
        /*System.debug('1 -> ' + EncodingUtil.urlDecode(base64Data,'UTF-8'));
        System.debug('2 -> ' + base64Data2);*/
        String base64Data3 = base64Data2.remove('data:image/png;base64,');
        
        ContentVersion cv = new ContentVersion();
        cv.Title = filename;
        cv.ContentDocumentId = contentDocumentId;  
        cv.VersionData = EncodingUtil.base64Decode(base64Data3);
        cv.PathOnClient = filename+'.png';
        cv.ContentLocation = 'S';
        cv.FirstPublishLocationId = recId;

        insert cv;

        return cv.Id;
        
    }
}