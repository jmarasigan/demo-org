public with sharing class ContactWithTabbedRelatedListCtrl {

    @AuraEnabled
    public static Contact getContact(String recId) {
        
        if (String.isNotBlank(recId)) {
            
            return [SELECT Id, Name, Salutation, Title, Phone, MobilePhone, Email,
                           AccountId, Account.Name,  
                    	   (SELECT Id, Subject, ActivityDate, Status, IsTask,
                    	           WhatId, What.Name,  OwnerId, Owner.Name 
                    	    FROM OpenActivities)
                    FROM Contact
                    WHERE (Id = :recId)];
        } else {
            
            return null;
        }
    }
    
    @AuraEnabled
    public static Boolean deleteContact(String recId){
        
        if(String.isNotBlank(recId)){
            
            Contact con = new Contact(Id=recId);
            delete con;
            return true;
            
        }else{
            return null;
        }
    }
}