@isTest
private class EditLeadCtrExt_Test {

    @isTest static void createNew(){
        
        Test.setCurrentPage(Page.EditLead);
        
        Lead newLead = new Lead();
        ApexPages.StandardController leadStdCon = new ApexPages.StandardController(newLead);
        EditLeadCtrExt leadConExt = new EditLeadCtrExt(leadStdCon);
        
        system.assertEquals('Change me' , newLead.FirstName);
        
        newLead.FirstName = 'ThisIsANewLeadFN';
        newLead.LastName = 'ThisIsANewLeadLN';
        newLead.Company ='ThisIsANewLeadCompany';
        newLead.Status ='Open - Not Contacted';
        
        leadConExt.myCustomValue = ' New Value';
        
        Test.startTest();
        	PageReference pageRef = leadConExt.myCustomSave();
        Test.stopTest();
        
        List <Lead> newLeadRecs = [SELECT Id, FirstName, LastName, Company, Status FROM Lead WHERE (Id=:newLead.Id)];
        
        for(Lead lr: newLeadRecs){
            
            system.assertEquals(newLead.FirstName , lr.FirstName);
            System.assertEquals(newLead.LastName, lr.LastName);
            System.assertEquals(newLead.Company, lr.Company);
            System.assertEquals(newLead.Status, lr.Status);
        }
        system.assert(pageRef.getUrl().contains('/' + newLead.Id));
    }
    
    @isTest
    
    public static void checkerrormessage(){
        
         Test.setCurrentPage(Page.EditLead);
        
        Lead newLead = new Lead();
        ApexPages.StandardController leadStdCon = new ApexPages.StandardController(newLead);
        EditLeadCtrExt leadConExt = new EditLeadCtrExt(leadStdCon);
        
        system.assertEquals('Change me' , newLead.FirstName);
        
        newLead.FirstName = '';
        newLead.LastName = 'ThisIsANewLeadLN';
        newLead.Company ='ThisIsANewLeadCompany';
        newLead.Status ='Open - Not Contacted';
        
        leadConExt.myCustomValue = ' New Value';
        
        Test.startTest();
        	PageReference pageRef = leadConExt.myCustomSave();
        Test.stopTest();
        
        ApexPages.Message fnError;
        ApexPages.Message wError;
        ApexPages.Message wWarning;
        for(ApexPages.Message apm: ApexPages.getMessages()){
            
            if(apm.getSeverity() == ApexPAges.Severity.ERROR){
                
                if(apm.getDetail().contains('Error1')){
                    
                    fnError = apm;
                    
                }else{
                    
                    wError = apm;
                }
                
            }else if(apm.getSeverity() == ApexPAges.Severity.WARNING){
                wWarning = apm;
            }
        }
        
        system.assert(pageRef == null);
        
        system.assert(fnError != null);
        system.assert(fnError.getDetail().contains('Error1'));
        
        system.assert(wError != null);
        system.assert(wError.getDetail().contains('Error2'));
        
        system.assert(wWarning != null);
        system.assert(wWarning.getDetail().contains('WARNING'));
    }
    
    
}