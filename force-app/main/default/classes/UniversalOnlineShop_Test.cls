@isTest
private class UniversalOnlineShop_Test {
	
	
	@TestSetUp static void setUp(){
		TestData_Test.setUpData();
	}

 @isTest static  void getProductList() {
          
          
        
        Test.startTest(); 
        UniversalOnlineShop.getProductItems();
        Test.stopTest();     
        
        List <Product2> prod = [SELECT Id, Name, ProductCode FROM Product2 WHERE (ProductCode='prod1')];   
        for(Product2 prods: prod){
        	system.assertEquals(prods.ProductCode,'prod1');
        }  
      }
      
      
      @isTest static  void submitOrder(){
        
        Account acct = TestData_Test.getAccount(); 
        
      String accnt = acct.id;
            
       List<UniversalOnlineShop.inputOrderRequestDetails> ordr = new List<UniversalOnlineShop.inputOrderRequestDetails>();
        
        UniversalOnlineShop.inputOrderRequestDetails ordrU =new UniversalOnlineShop.inputOrderRequestDetails();
        ordrU.productCode2 ='prod1';
        ordrU.Quantity = 1;
        
        ordr.add(ordrU);
        
        Test.startTest(); 
        UniversalOnlineShop.getsubmitOrder(accnt,ordr);
        Test.stopTest();     

        //system.assertEquals();    
    }
    
          @isTest static  void submitOrderInvalidAccount(){
        
       // Account acct = TestData_Test.getAccount(); 
        
      String accnt = 'Invalid';
            
       List<UniversalOnlineShop.inputOrderRequestDetails> ordr = new List<UniversalOnlineShop.inputOrderRequestDetails>();
        
        UniversalOnlineShop.inputOrderRequestDetails ordrU =new UniversalOnlineShop.inputOrderRequestDetails();
        ordrU.productCode2 ='prod1';
        ordrU.Quantity = 1;
        
        ordr.add(ordrU);
        
        Test.startTest(); 
        UniversalOnlineShop.getsubmitOrder(accnt,ordr);
        Test.stopTest();
        
      /*  List <Account> act = [SELECT Id FROM Account];
        
        for(Account acc: act){
        	System.assertEquals(acc.Id, accnt,'Invalid Account Specified');
        }*/
                 
    }
      
       @isTest static  void submitOrderInvalidQuantity(){
        
        Account acct = TestData_Test.getAccount();
        
      String accnt = acct.id;     
            
       List<UniversalOnlineShop.inputOrderRequestDetails> ordr = new List<UniversalOnlineShop.inputOrderRequestDetails>();
        
        UniversalOnlineShop.inputOrderRequestDetails ordrU = new UniversalOnlineShop.inputOrderRequestDetails();
        ordrU.productCode2 ='prod1';
        ordrU.Quantity = 1.5;
        
        ordr.add(ordrU);
        
        Test.startTest(); 
        UniversalOnlineShop.getsubmitOrder(accnt,ordr);
        Test.stopTest();   
        }     
    
    
        @isTest static  void submitOrderInvalidCode(){
        
        Account acct = TestData_Test.getAccount();
        
      	String accnt = acct.id;     
            
       List<UniversalOnlineShop.inputOrderRequestDetails> ordr = new List<UniversalOnlineShop.inputOrderRequestDetails>();
        
        UniversalOnlineShop.inputOrderRequestDetails ordrU =new UniversalOnlineShop.inputOrderRequestDetails();
        ordrU.productCode2 ='Wrong';
        ordrU.Quantity = 1;
        
        ordr.add(ordrU);
        
        Test.startTest(); 
        UniversalOnlineShop.getsubmitOrder(accnt,ordr);
        Test.stopTest();    
    
    }
    
    
    
    
    
    
}