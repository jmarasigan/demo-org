global class SampleAccountWebService {
    
    global class AccountQueryOptions{
    	
    	webService string name;
    	webService string website;
    	webService String billCountry;
    }
    global class AccountDetails{
    	
    	webService String accountId;
    	webService string name;
    	webService string website;
    	webService String billingCountry;
    }
    
    webService static String getAccountName(String acctId){
    	
    	system.assert(acctId !=null);
    	
    	List <Account> accts = [SELECT Id, Name FROM Account WHERE (Id=:acctId)];
    	
    	if(!accts.isEmpty()){
    		
    		return accts.get(0).Name;
    	}
    	else{
    		return 'INVALID ID';
    	}
    	
    }
    
    webService static List<AccountDetails> getAccountDetails(AccountQueryOptions queryOpts){
    	
    	system.assert(queryOpts != null);
    	
    	String queryStr = 'SELECT Id, Name, Website, BillingCountry ' +
    						'FROM Account WHERE ';
    						
    	List<String> queryConditions = new List<String>();					
    						
    	if(String.isNotBlank(queryOpts.name)){
    		
    		queryConditions.add('(Name LIKE \'%' + queryopts.name + '%\')');
    		
    	}
    	if(String.isNotBlank(queryOpts.website)){
    		
    		queryConditions.add('(Website LIKE \'%' + queryopts.website + '%\')');
    		
    	}
    		if(String.isNotBlank(queryOpts.billCountry)){
    		
    		queryConditions.add('(BillingCountry = \'' + queryopts.billCountry + '\')');
    		
    	}
    	
    	List<AccountDetails> accList = new List<AccountDetails>();
    	
    	if(!queryConditions.isEmpty()){
    		
    		queryStr += String.join(queryConditions, ' AND ');
    		
    		List <Account> accts = Database.query(queryStr);
    		
    		for (Account acctRec: accts){
    			
    			AccountDetails acctDet = new AccountDetails();
    			acctDet.accountId = acctRec.Id;
    			acctDet.name = acctRec.Name;
    			acctDet.website = acctRec.Website;
    			acctDet.billingCountry = acctRec.BillingCountry;

				accList.add(acctDet);
    		}
    	
    	}
    	
    	return accList;
    }
}