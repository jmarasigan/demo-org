public class SampleRestCallout {
	
	public static final string CS_JSON_NAME ='JSONEndPoint';
	public static final string CS_XML_NAME ='XMLEndPoint';
	
	//public static Boolean useJSON = true;

	public static HttpResponse callgoogleMapsApi(){
		
		HierarchyCustomSetting__c userSettings = HierarchyCustomSetting__c.getInstance(UserInfo.getUserId());
		
		ListCustomSetting__c wsSettings;
		
		if(userSettings.useJSON__c){
			
			wsSettings = ListCustomSetting__c.getValues(CS_JSON_NAME);
			
		}else{
			wsSettings = ListCustomSetting__c.getValues(CS_XML_NAME);
		}
		
		HttpRequest mapRequest = new HttpRequest();
		mapRequest.setEndpoint(wsSettings.EndPoint__c);
		mapRequest.setMethod('GET');
		mapRequest.setHeader('Content-Type', wsSettings.Content_Type__c);
		mapRequest.setHeader('Accept', wsSettings.Accept__c);
		
		Http wsManager = new Http();
		HttpResponse mapResponse = wsManager.send(mapRequest);
		system.debug(mapResponse.getBody());
		return mapResponse;		
	}


}