@isTest
private class NewAccountAndContact_Test {
	
@TestSetUp static void setUp(){
	
		DataFactor_Test.createContract();
}
	
	@isTest static void createnew(){
        
        Test.setCurrentPage(Page.NewAccountAndContact);
 
        Account newAcc = new Account();
        Contact newCon = new Contact();
        
        ApexPages.StandardController accStdCtr = new ApexPages.StandardController(newAcc);
        NewAccountAndContactCtrExt accConExt = new NewAccountAndContactCtrExt(accStdCtr);
               
        newAcc.Name='Sample';
        newAcc.BillingStreet='Street';
        newAcc.BillingCity = 'City';
        newAcc.BillingState = 'State';
        newAcc.BillingPostalCode = 'Postal Code';
        newAcc.BillingCountry = 'Country';
        newCon.LastName= 'Sample';
        newCon.Email = 'sample@email.com';
        
        accConExt.acct = newAcc;
        accConExt.cont = newCon;
        
        Test.startTest();
        	PageReference pageRef = accConExt.save();
        Test.stopTest();
        List <Account> acc = [SELECT Id, Name FROM Account WHERE (Name=:newAcc.Name)];
        
           for(Account accs: acc){
        	system.assertEquals(newAcc.Name,accs.Name);
        } 
        
        List<Contact> cont = [SELECT Id,LastName, MailingStreet, MailingCity, MailingState,MailingPostalCode, MailingCountry
        					  FROM Contact WHERE (LastName=:newCon.LastName)];
        for(Contact conts : cont){
			       system.assertEquals(newCon.LastName, conts.LastName);
			       system.assertEquals(newAcc.BillingStreet, conts.MailingStreet);
			       system.assertEquals(newAcc.BillingCity, conts.MailingCity);
			       system.assertEquals(newAcc.BillingState, conts.MailingState);
			       system.assertEquals(newAcc.BillingPostalCode, conts.MailingPostalCode);
			       system.assertEquals(newAcc.BillingCountry, conts.MailingCountry);
			       
			       
        }
        system.assert(pageRef.getUrl().contains('/' + newAcc.Id));
    }
    
    	@isTest static void createnewwithorder(){
    		
    		      Product2 prod = new Product2();
      prod.Name='prod1';
      prod.Family='prod1';
      prod.IsActive=true;
      prod.ProductCode='prod1';
      prod.Quantity_In_Stock__c = 1;
        insert prod;
        
        PriceBookEntry priceentry = new PriceBookEntry();
            priceentry.Pricebook2Id = Test.getStandardPricebookId(); 
            priceentry.Product2Id = prod.Id;
            priceentry.UnitPrice = 99;
            priceentry.IsActive = true;
        

        insert priceentry; 
        
        Test.setCurrentPage(Page.NewAccountAndContact);
 
        Account newAcc = new Account();
        Contact newCon = new Contact();

        ApexPages.StandardController accStdCtr = new ApexPages.StandardController(newAcc);
        NewAccountAndContactCtrExt accConExt = new NewAccountAndContactCtrExt(accStdCtr);
              
        newAcc.Name='Sample';
        newAcc.BillingStreet='Street';
        newAcc.BillingCity = 'City';
        newAcc.BillingState = 'State';
        newAcc.BillingPostalCode = 'Postal Code';
        newAcc.BillingCountry = 'Country';
        newCon.LastName= 'Sample';
        newCon.Email = 'sample@email.com';
        accConExt.freegift = true;
        accConExt.pri.Product2Id = prod.Id;
        accConExt.pri = priceentry; 
        
        accConExt.acct = newAcc;
        accConExt.cont = newCon;
        
        Test.startTest();
        	PageReference pageRef = accConExt.save();
        Test.stopTest();
        
       List <Account> acc = [SELECT Id, Name FROM Account WHERE (Name=:newAcc.Name)];
        
           for(Account accs: acc){
        	system.assertEquals(newAcc.Name,accs.Name);
        } 
        
        List<Contact> cont = [SELECT Id,LastName, MailingStreet, MailingCity, MailingState,MailingPostalCode, MailingCountry
        					  FROM Contact WHERE (LastName=:newCon.LastName)];
        for(Contact conts : cont){
			       system.assertEquals(newCon.LastName, conts.LastName);
			       system.assertEquals(newAcc.BillingStreet, conts.MailingStreet);
			       system.assertEquals(newAcc.BillingCity, conts.MailingCity);
			       system.assertEquals(newAcc.BillingState, conts.MailingState);
			       system.assertEquals(newAcc.BillingPostalCode, conts.MailingPostalCode);
			       system.assertEquals(newAcc.BillingCountry, conts.MailingCountry);
			       
			       
        }
        
        List<Order> ords = [SELECT Id,Name FROM Order WHERE (Name=:newAcc.Name)];
        for(Order ordss : ords){
			       system.assertEquals(newAcc.Name, ordss.Name);
        }
         system.assert(pageRef.getUrl().contains('/' + newAcc.Id));
    }
    

    @isTest static void greaterThan(){
        
        
      Product2 prod = new Product2();
      prod.Name='prod1';
      prod.Family='prod1';
      prod.IsActive=true;
      prod.ProductCode='prod1';
      prod.Quantity_In_Stock__c = 1;
        insert prod;
        
        PriceBookEntry priceentry = new PriceBookEntry();
            priceentry.Pricebook2Id = Test.getStandardPricebookId(); 
            priceentry.Product2Id = prod.Id;
            priceentry.UnitPrice = 999;
            priceentry.IsActive = true;
        

        insert priceentry; 
        
        Test.setCurrentPage(Page.NewAccountAndContact);
        
        Account newAcc = new Account();
        Contact newCon = new Contact();
        
        ApexPages.StandardController accStdCtr = new ApexPages.StandardController(newAcc);
        NewAccountAndContactCtrExt accConExt = new NewAccountAndContactCtrExt(accStdCtr);
               
        newAcc.Name = 'Sample';
        newCon.LastName= 'Sample';
        newCon.Email = 'sample@email.com';
        accConExt.freegift = true;
        accConExt.pri.Product2Id = prod.Id;
        accConExt.pri = priceentry; 

        
        
        Test.startTest();
        	accConExt.save();
        Test.stopTest();
        
        ApexPages.Message msgError;
        
           for(ApexPages.Message apm: ApexPages.getMessages()){
        	if(apm.getSeverity() == ApexPAges.Severity.ERROR){
                if(apm.getDetail().contains('Invalid Product. A free gift may only be less than $100')){
                    
                    msgError = apm;
                    
                }
            }
        }
        system.assert(msgError != null);
        system.assert(msgError.getDetail().contains('Invalid Product. A free gift may only be less than $100'));
    }
    
        @isTest static void noStock(){
        
        Test.setCurrentPage(Page.NewAccountAndContact);
      
      Product2 prod = new Product2();
      prod.Name='prod1';
      prod.Family='prod1';
      prod.IsActive=true;
      prod.ProductCode='prod1';
      prod.Quantity_In_Stock__c = 0;
        insert prod;
        
        PriceBookEntry priceentry = new PriceBookEntry();
            priceentry.Pricebook2Id = Test.getStandardPricebookId(); 
            priceentry.Product2Id = prod.Id;
            priceentry.UnitPrice = 99;
            priceentry.IsActive = true;
        

        insert priceentry; 
        
        
        Account newAcc = new Account();
        Contact newCon = new Contact();
        
        ApexPages.StandardController accStdCtr = new ApexPages.StandardController(newAcc);
        NewAccountAndContactCtrExt accConExt = new NewAccountAndContactCtrExt(accStdCtr);
        
            
        newAcc.Name = 'Sample';
        newCon.LastName= 'Sample';
        newCon.Email = 'sample@email.com';
        accConExt.freegift = true;
        accConExt.pri.Product2Id = prod.Id;
        
        
        Test.startTest();
        	accConExt.save();
        Test.stopTest();
        
                ApexPages.Message msgError;
           for(ApexPages.Message apm: ApexPages.getMessages()){
        	if(apm.getSeverity() == ApexPAges.Severity.ERROR){
                if(apm.getDetail().contains('Invalid Product. This product is out of stock')){
                    
                    msgError = apm;
                    
                }
            }
        }
        system.assert(msgError != null);
        system.assert(msgError.getDetail().contains('Invalid Product. This product is out of stock'));
    }
    
    
    @isTest static void noProduct(){
        
        Test.setCurrentPage(Page.NewAccountAndContact);
        
        Account newAcc = new Account();
        Contact newCon = new Contact();
        ApexPages.StandardController accStdCtr = new ApexPages.StandardController(newAcc);
        NewAccountAndContactCtrExt accConExt = new NewAccountAndContactCtrExt(accStdCtr);
        
        
        newAcc.Name = 'Sample';
		newCon.LastName = 'Sample';
        newCon.Email = 'sample@email.com';
        accConExt.freegift = true;
        accConExt.pri.Product2Id = null;
        
        ApexPages.Message msgError;
        Test.startTest();
        	accConExt.save();
        Test.stopTest();
        
        for(ApexPages.Message apm: ApexPages.getMessages()){
        	if(apm.getSeverity() == ApexPAges.Severity.ERROR){
                if(apm.getDetail().contains('No product specified')){
                    
                    msgError = apm;
                    
                }
            }
        }
        system.assert(msgError != null);
        system.assert(msgError.getDetail().contains('No product specified'));
    }
    
        @isTest static void trueGirftNoProd(){
        	
      Product2 prod = new Product2();
      prod.Name='prod1';
      prod.Family='prod1';
      prod.IsActive=true;
      prod.ProductCode='prod1';
      prod.Quantity_In_Stock__c = 0;
        insert prod;
        
      PriceBookEntry priceentry = new PriceBookEntry();
            priceentry.Pricebook2Id = Test.getStandardPricebookId(); 
            priceentry.Product2Id = prod.Id;
            priceentry.UnitPrice = 99;
            priceentry.IsActive = true;
        

        insert priceentry; 
        
        Test.setCurrentPage(Page.NewAccountAndContact);
        
        Account newAcc = new Account();
        Contact newCon = new Contact();
        ApexPages.StandardController accStdCtr = new ApexPages.StandardController(newAcc);
        NewAccountAndContactCtrExt accConExt = new NewAccountAndContactCtrExt(accStdCtr);
        
        
        newAcc.Name = 'Sample';
		newCon.LastName = 'Sample';
        newCon.Email = 'sample@email.com';
        accConExt.freegift = false;
        accConExt.pri.Product2Id = prod.Id;
        
        accConExt.acct = newAcc;
        accConExt.cont = newCon;
        
        ApexPages.Message msgError;
        Test.startTest();
        	accConExt.save();
        Test.stopTest();
        
        List <Account> acc = [SELECT Id, Name FROM Account WHERE (Name=:newAcc.Name)];
        
           for(Account accs: acc){
        	system.assertEquals(accs.Name,newAcc.Name);
        } 
        
        List<Contact> cont = [SELECT Id,LastName FROM Contact WHERE (LastName=:newCon.LastName)];
        for(Contact conts : cont){
			       system.assertEquals(newCon.LastName, conts.LastName);
        }
        
        List<Order> ords = [SELECT Id,Name FROM Order WHERE (Name=:newAcc.Name)];
        for(Order ordss : ords){
			       system.assert(ordss == null);
        }
    }
    
    @isTest static void cancelNew(){
    	
        Test.setCurrentPage(Page.NewAccountAndContact);
        
        Account newAcc = new Account();
        
        ApexPages.StandardController accStdCtr = new ApexPages.StandardController(newAcc);
        NewAccountAndContactCtrExt accConExt = new NewAccountAndContactCtrExt(accStdCtr);
        
        Test.startTest();
        	PageReference pageRef = accConExt.cancel();
        Test.stopTest();
        
        system.assert(pageRef != null);
        system.assertEquals('https://ap5.salesforce.com/001/o', PageRef.getUrl());
       

    }
    
 
    
}