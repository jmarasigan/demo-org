@isTest
public class ViewRecordListing_Test {
    
    

    @isTest static void getMultipleAccount(){
        
        insert SetUpData_Test.getPositiveBulkAccount();
        
                
        Test.setCurrentPage(Page.ViewRecordListing);
        

        ViewRecordListingCtrl controller = new ViewRecordListingCtrl();
        
        controller = new ViewRecordListingCtrl();
        controller.selectedObj = 'Account';
        
        Test.startTest();
       	  controller.getRecords();
      	  controller.viewRecords();
      	  controller.goToFirst();
      	  controller.goToNext();  
      	  controller.goToPrevious();
       	  controller.goToLast();
        Test.stopTest();
        
        system.assert(controller.acctList != null);
        system.assert(controller.MAX_PAGE_SIZE == 5);
        
       
    }
    
        @isTest static void getDeffiveMultipleAccount(){
        
        insert SetUpData_Test.getPositiveBulkAccount();
        
                
        Test.setCurrentPage(Page.ViewRecordListing);
        

        ViewRecordListingCtrl controller = new ViewRecordListingCtrl();
        
        controller = new ViewRecordListingCtrl();
        controller.selectedObj = 'Account';
        controller.MAX_PAGE_SIZE = 10;
        
        Test.startTest();
       	  controller.getRecords();
      	  controller.viewRecords();
      	  controller.goToFirst();
      	  controller.goToNext();  
      	  controller.goToPrevious();
       	  controller.goToLast();
        Test.stopTest();
            
            system.assert(controller.acctList != null);
            system.assert(controller.MAX_PAGE_SIZE == 10);
        
       
    }
    
    
    
        @isTest static void getContact(){
            
         insert SetUpData_Test.getPositiveSingleAccount();
		 insert SetUpData_Test.getBulkContact();

                
        Test.setCurrentPage(Page.ViewRecordListing);
        ViewRecordListingCtrl controller = new ViewRecordListingCtrl();
        
        controller = new ViewRecordListingCtrl();
        controller.selectedObj = 'Contact';
        controller.MAX_PAGE_SIZE = 15;
        
        Test.startTest();
       		controller.getRecords();
            controller.viewRecords();
            controller.goToNext();
            controller.goToLast();
            controller.goToFirst();
            controller.goToPrevious();
        Test.stopTest();
            
           system.assert(controller.contList != null);
            system.assert(controller.MAX_PAGE_SIZE == 15);
    }
    
    
             @isTest static void getDeftwentyOpportunities(){
            
             
            List<Opportunity> opp = new List<Opportunity>();
            
             for(Integer i=0;i<100;i++) {
                        Opportunity Oppo = new Opportunity(Name='Test' + i,
                                              CloseDate=system.today(),
                                              StageName ='Prospecting');
                 opp.add(Oppo);
             }
             
             
             
       insert opp;
                
        Test.setCurrentPage(Page.ViewRecordListing);
        ViewRecordListingCtrl controller = new ViewRecordListingCtrl();
        
        controller = new ViewRecordListingCtrl();
        controller.selectedObj = 'Opportunities';    
       controller.MAX_PAGE_SIZE = 20;
        
        Test.startTest();
        	controller.getRecords();
            controller.viewRecords();
            controller.showRecords();
            controller.goToNext();
            controller.goToLast();
            controller.goToFirst();
            controller.goToPrevious();
        Test.stopTest();
                 
                 system.assert(controller.oppList !=null);
                 system.assert(controller.MAX_PAGE_SIZE == 20);
    }
    
         @isTest static void getOpportunities(){
             
            insert SetUpData_Test.getPositiveSingleAccount();

            List <Account> acc = [SELECT Id, Name FROM Account WHERE (Name='Test')];
            Account accs =acc.get(0);
             
            List<Opportunity> opp = new List<Opportunity>();
             
            
             for(Integer i=0;i<100;i++) {
                        Opportunity Oppo = new Opportunity(Name='Test' + i,
                                              			   AccountId = accs.Id,             
                                             		       CloseDate=system.today(),
                                              			   StageName ='Prospecting');
                 opp.add(Oppo);
             }
             
             
             
       insert opp;
                
        Test.setCurrentPage(Page.ViewRecordListing);
        ViewRecordListingCtrl controller = new ViewRecordListingCtrl();
        
        controller = new ViewRecordListingCtrl();
        controller.selectedObj = 'Opportunities';
        controller.MAX_PAGE_SIZE = 25;

        Test.startTest();
        	controller.getRecords();
            controller.viewRecords();
            controller.showRecords();
            controller.goToNext();
            controller.goToLast();
            controller.goToFirst();
            controller.goToPrevious();

        Test.stopTest();
             
             system.assert(controller.oppList !=null);
             system.assert(controller.MAX_PAGE_SIZE == 25);
             
             
    }
    
    
}