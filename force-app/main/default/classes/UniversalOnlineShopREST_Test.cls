@isTest
private class UniversalOnlineShopREST_Test {
	
@TestSetUp static void setUp(){
	
		TestData_Test.setUpData();
}

@isTest static void getProducts(){
	
        Test.startTest(); 
        UniversalOnlineShopREST.getProductItems();
        Test.stopTest();     
        
        List <Product2> prod = [SELECT Id, Name, ProductCode FROM Product2 WHERE (ProductCode='prod1')]; 
          
        for(Product2 prods: prod){
        	system.assertEquals(prods.ProductCode,'prod1');
        } 

}

      @isTest static  void submitOrder(){
        
        Account acct = TestData_Test.getAccount(); 
        
        String accnt = acct.id;
            
        List<UniversalOnlineShopREST.inputOrderRequestDetails> ordr = new List<UniversalOnlineShopREST.inputOrderRequestDetails>();
        
        UniversalOnlineShopREST.inputOrderRequestDetails ordrU =new UniversalOnlineShopREST.inputOrderRequestDetails();
        ordrU.productCode2 ='prod1';
        ordrU.quantity = 1;
        
        ordr.add(ordrU);
        
        Test.startTest(); 
        UniversalOnlineShopREST.getsubmitOrder(accnt,ordr);
        Test.stopTest(); 
        
        List<Order> ordchk = [SELECT Id, AccountId FROM Order WHERE (Status = 'Activated')];
        Order orc = ordchk.get(0);
        system.assertEquals(orc.AccountId, acct.id);
           
}
    
@isTest static  void submitOrderInvalidAccount(){
        
       // Account acct = TestData_Test.getAccount(); 
        
        String accnt = 'Invalid';
            
        List<UniversalOnlineShopREST.inputOrderRequestDetails> ordr = new List<UniversalOnlineShopREST.inputOrderRequestDetails>();
       
        List<UniversalOnlineShopREST.OrderResponseDetails> messages;
        
        UniversalOnlineShopREST.inputOrderRequestDetails ordrU =new UniversalOnlineShopREST.inputOrderRequestDetails();
        ordrU.productCode2 ='prod1';
        ordrU.Quantity = 1;
        
        ordr.add(ordrU);
        
        Test.startTest();    
        
        messages = UniversalOnlineShopREST.getsubmitOrder(accnt,ordr);
         
        Test.stopTest();
        
        String mess = messages.get(0).message;
		system.assertEquals(mess,'Invalid Account Specified');
		
                 
}
    
    
@isTest static  void submitOrderInvalidQuantity(){
        
       Account acct = TestData_Test.getAccount();
        
       String accnt = acct.id;     
      
       List<UniversalOnlineShopREST.OrderResponseDetails> messages;
            
       List<UniversalOnlineShopREST.inputOrderRequestDetails> ordr = new List<UniversalOnlineShopREST.inputOrderRequestDetails>();
        
        UniversalOnlineShopREST.inputOrderRequestDetails ordrU = new UniversalOnlineShopREST.inputOrderRequestDetails();
        ordrU.productCode2 ='prod1';
        ordrU.Quantity = 1.5;
        
        ordr.add(ordrU);
        
        Test.startTest(); 
        messages = UniversalOnlineShopREST.getsubmitOrder(accnt,ordr);
        Test.stopTest();   
        
        String mess = messages.get(0).message;
		system.assertEquals(mess,'Invalid Quantity Specified');
		
        }     
    
    
        @isTest static  void submitOrderInvalidCode(){
        
        Account acct = TestData_Test.getAccount();
        
      	String accnt = acct.id;     
            
        List<UniversalOnlineShopREST.OrderResponseDetails> messages;    
            
        List<UniversalOnlineShopREST.inputOrderRequestDetails> ordr = new List<UniversalOnlineShopREST.inputOrderRequestDetails>();
        
        UniversalOnlineShopREST.inputOrderRequestDetails ordrU =new UniversalOnlineShopREST.inputOrderRequestDetails();
        ordrU.productCode2 ='Invalid';
        ordrU.Quantity = 1;
        
        ordr.add(ordrU);
        
        Test.startTest(); 
        messages = UniversalOnlineShopREST.getsubmitOrder(accnt,ordr);
        Test.stopTest();    
    
        String mess = messages.get(0).message;
		system.assertEquals(mess,'Invalid Product Code Specified');
		
    
    }
}