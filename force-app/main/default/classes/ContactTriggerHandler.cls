public class ContactTriggerHandler {
    
    public static void doBeforeInsert(List<Contact> NewContact){
        
       Set<Id>contactId = new Set<Id>();

        for(Contact con: NewContact){
        if(con.Primary__c == TRUE){
            contactid.add(con.AccountId);
        }
        
        }
        
               Map <Id,Account> checkacc = new Map<Id,Account>([SELECT Id
                                                             FROM Account
                                                             WHERE(Id IN (
                                                                          SELECT AccountId
                                                                          FROM Contact
                                                                          WHERE (AccountId IN:contactid)
                                                                          AND(Primary__c = TRUE)
                                                                          )
                                                                  )
                                                             ]);
      
        for(Contact NewContacts : NewContact){
             if(checkacc.containsKey(NewContacts.AccountId)){
                 NewContacts.addError('Invalid Contact. An Account may only have 1 Primary Contact.');
             }
         }          
 }
    
    
    public static void doBeforeUpdate(List<Contact> NewContact, Map<Id, Contact> oldContactMap){
        
              Set<Id>contactId = new Set<Id>();
     
         
        for(Contact con: NewContact){
            
            Contact old = oldContactMap.get(con.Id);
            
        if(con.Primary__c == TRUE && (old.Primary__c!=con.Primary__c || old.AccountId!=con.AccountId)){
            contactid.add(con.AccountId);
        }
        
        }
        
               Map <Id,Account> checkacc = new Map<Id,Account>([SELECT Id
                                                             FROM Account
                                                             WHERE(Id IN (
                                                                          SELECT AccountId
                                                                          FROM Contact
                                                                          WHERE (AccountId IN:contactid)
                                                                          AND(Primary__c = TRUE)
                                                                          )
                                                                  )
                                                             ]);
      
        for(Contact NewContacts : NewContact){
             if(checkacc.containsKey(NewContacts.AccountId)){
                 NewContacts.addError('Invalid Contact. An Account may only have 1 Primary Contact.');
             }
         } 
        
    }   
}