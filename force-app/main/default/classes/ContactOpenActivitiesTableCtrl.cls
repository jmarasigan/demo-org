public with sharing class ContactOpenActivitiesTableCtrl {

    @AuraEnabled
    public static List<OpenActivity> getOpenActivities(String recId){
        
        if(String.isNotBlank(recId)){
            
            List<Contact> contOA = [SELECT Id, (SELECT Id, Subject, WhatId, WhoId, ActivityDate, Status,
                                                OwnerId FROM OpenActivities)
									FROM Contact 
									WHERE (Id = :recId)];
            return contOA.get(0).OpenActivities;
        }else{
            return null;
        }
        
    }
}