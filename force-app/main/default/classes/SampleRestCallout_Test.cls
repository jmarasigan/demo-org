@isTest
public class SampleRestCallout_Test {
	
	//call using static uploaded values    
	@testSetup static void setupTestData(){
		
		Test.loadData(HierarchyCustomSetting__c.sObjectType, 'TestHierarchyCustomSettingValues');
		Test.loadData(ListCustomSetting__c.sObjectType, 'TestListCustomSettingValues');
		
	}    
	 @isTest static void callUsingJSON(){
	    //ListCustomSetting__c wsSetting = ListCustomSetting__c.getValues(SampleRestCallout.CS_JSON_NAME);
        //System.debug('>>> wsSetting : ' + wsSetting);
        
        HierarchyCustomSetting__c userSetting = HierarchyCustomSetting__c.getInstance(UserInfo.getUserId());
        userSetting.UseJSON__c = true;
        update userSetting;
        
        Test.startTest();
        Test.setMock(HttpCallOutMock.class, new SampleRestCalloutMockTest2());
        HttpResponse wsResponse = SampleRestCallout.callGoogleMapsApi();
        Test.stopTest();
        
        ListCustomSetting__c jsonEndpoint = ListCustomSetting__c.getValues(SampleRestCallout.CS_JSON_NAME);
        System.assertEquals(jsonEndpoint.Content_Type__c, wsResponse.getHeader('Content-Type'));
        System.assert(wsResponse.getBody().contains('ChIJ4UnHqgjJlzMRjEf1FFBMrEU'));
    }
	    	 @isTest static void callUsingXML(){
	    //ListCustomSetting__c wsSetting = ListCustomSetting__c.getValues(SampleRestCallout.CS_JSON_NAME);
        //System.debug('>>> wsSetting : ' + wsSetting);
        
        HierarchyCustomSetting__c userSetting = HierarchyCustomSetting__c.getInstance(UserInfo.getUserId());
        userSetting.UseJSON__c = false;
        update userSetting;
        
        Test.startTest();
        Test.setMock(HttpCallOutMock.class, new SampleRestCalloutMockTest2());
        HttpResponse wsResponse = SampleRestCallout.callGoogleMapsApi();
        Test.stopTest();
        
        ListCustomSetting__c jsonEndpoint = ListCustomSetting__c.getValues(SampleRestCallout.CS_XML_NAME);
        System.assertEquals(jsonEndpoint.Content_Type__c, wsResponse.getHeader('Content-Type'));
        System.assert(wsResponse.getBody().contains('ChIJ4UnHqgjJlzMRjEf1FFBMrEU'));
    }
	
	    ///////////////////////////////////////////////////////
	    
	    
    /*@isTest static void json(){
    	
		ListCustomSetting__c sList = new ListCustomSetting__c();
		sList.Name = 'JSONEndPoint';
		sList.Accept__c = 'application/json';
		sList.Content_Type__c = 'application/json; charset=UTF-8';
		sList.EndPoint__c ='https://maps.googleapis.com/maps/api/geocode/json?address=139+Valero,+Makati,+1227+Kalakhang+Maynila&sensor=false';
		
		HierarchyCustomSetting__c sCus = new HierarchyCustomSetting__c();
		sCus.UseJSON__c = true;
		    
    	insert sCus;
    	insert sList;
    	
    	Test.startTest();
    	
    	Test.setMock(HttpCallOutMock.class, new SampleRestCalloutMock_Test());
    	HttpResponse res = SampleRestCallout.callgoogleMapsApi();

    	Test.stopTest();
    	
    	system.assertEquals(res.getBody(),'json');
    }
    
        @isTest static void xml(){
    	
		ListCustomSetting__c sList = new ListCustomSetting__c();
		sList.Name = 'XMLEndPoint';
		sList.Accept__c = 'application/xml';
		sList.Content_Type__c = 'application/xml; charset=UTF-8';
		sList.EndPoint__c ='https://maps.googleapis.com/maps/api/geocode/xml?address=139+Valero,+Makati,+1227+Kalakhang+Maynila&sensor=false';
		
		HierarchyCustomSetting__c sCus = new HierarchyCustomSetting__c();
		sCus.UseJSON__c = false;
		
		insert sCus;
    	insert sList;
    	
    	Test.startTest();
    	
    	Test.setMock(HttpCallOutMock.class, new SampleRestCalloutMock_Test());
    	HttpResponse res = SampleRestCallout.callgoogleMapsApi();

    	Test.stopTest();
    	
    	system.assertEquals(res.getBody(),'xml');
    }*/
}