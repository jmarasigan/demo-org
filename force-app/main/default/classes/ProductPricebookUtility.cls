public class ProductPricebookUtility {
    
    public static Id getStandardPricebookId() {
        
        Id spbId;
        if (Test.isRunningTest()) {
            
            spbId = Test.getStandardPricebookId();
        } else {
            
            List<Pricebook2> standardPB = [SELECT Id 
                                            FROM Pricebook2 
                                            WHERE (IsStandard = TRUE)
                                            AND (IsActive = TRUE)
                                            ];
                                            
            if (!standardPB.isEmpty()) {
                
                spbId = standardPB.get(0).Id;
            }        
        }

        return spbId;
    }
    
    public static List<PricebookEntry> getProductsWithPrice() {
        
        return [SELECT Id, UnitPrice, Product2Id, 
                        Product2.Name, Product2.ProductCode, 
                        Product2.Family, Product2.Quantity_In_Stock__c
                FROM PricebookEntry
                WHERE (Pricebook2Id = :getStandardPricebookId())
                AND (IsActive = TRUE)
                LIMIT 10000
                ];        
    }
    
    public static Map<String, PricebookEntry> getProductsMappedByCode() {
        
        Map<String, PricebookEntry> prodMap = new Map<String, PricebookEntry>();
        for (PricebookEntry pbe: getProductsWithPrice()) {
            
            prodMap.put(pbe.ProductCode, pbe);
        }
        
        return prodMap;
    }
    
}