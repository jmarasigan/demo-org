@isTest
global class SampleRestCalloutMockTest2 implements HttpCalloutMock {

	public HttpResponse respond(HttpRequest request){
		
		ListCustomSetting__c jsonEndpoint = ListCustomSetting__c.getValues(SampleRestCallout.CS_JSON_NAME);
		ListCustomSetting__c xmlEndpoint = ListCustomSetting__c.getValues(SampleRestCallout.CS_XML_NAME);
		
		HttpResponse wsResponse = new HttpResponse();
		
		if(request.getEndpoint().equals(jsonEndpoint.Endpoint__c)){
			wsResponse.setHeader('Content-Type', jsonEndpoint.Content_Type__c);
			wsResponse.setBody('{"status" : "OK", "results" : [{"place_id" : "ChIJ4UnHqgjJlzMRjEf1FFBMrEU"}]}');
			
			
		}else if(request.getEndpoint().equals(xmlEndpoint.Endpoint__c)){
			wsResponse.setHeader('Content-Type', xmlEndpoint.Content_Type__c);
			wsResponse.setBody('<GeocodeResponse><status>OK</status><result><place-id>ChIJ4UnHqgjJlzMRjEf1FFBMrEU</place_id></result></GeocodeResponse>');
		}
		return wsResponse;
	}
    
}