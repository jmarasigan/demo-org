@isTest
public  class DataFactor_Test {
    
    public static void setitup(){
        
        List <Product2> prod = createProduct2();
        insert prod; 
        
        
        
        List <PricebookEntry> priceentry = new List<PricebookEntry>();
        
        for(Product2 prods :prod){
            
            PricebookEntry priceent = new PricebookEntry();
            priceent.Pricebook2Id = Test.getStandardPricebookId(); 
            priceent.Product2Id = prods.Id;
            priceent.UnitPrice = 99;
            priceent.IsActive = true;
            
            priceentry.add(priceent);   
            
        }
        
        
        insert priceentry; 
        
        
        
    }
    
    public static List<Product2> createProduct2(){
        
        List<Product2> prods = new List<Product2>();
        
        Product2 prod1 = new Product2(Name = 'prod1',Family ='prod1', IsActive = true, ProductCode='prod1', Quantity_In_Stock__c = 1);
        prods.add(prod1);
        Product2 prod2 = new Product2(Name = 'prod2',Family ='prod2', IsActive = true, ProductCode='prod2', Quantity_In_Stock__c = 1);
        prods.add(prod2);
        Product2 prod3 = new Product2(Name = 'prod3',Family ='prod3', IsActive = true, ProductCode='prod3', Quantity_In_Stock__c = 1);
        prods.add(prod3);
        
        return(prods);
    }
    
    
    public static Contract createContract() {
        
        
        Account acct = SetUpData_Test.getPositiveSingleAccount();
        
        insert acct;
        
        Contract cont = new Contract (AccountId=acct.id , Status='Draft', StartDate = System.today(), ContractTerm= 6, PriceBook2Id =Test.getStandardPricebookId());
        
        insert cont;                 
        
        Contract conUpdate =[SELECT Status FROM Contract WHERE Id =:cont.Id];
        
        conUpdate.Status ='Activated';
        
        update conUpdate;
        
        return(cont);
    }
    
}