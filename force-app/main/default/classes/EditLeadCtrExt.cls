public class EditLeadCtrExt {

    public string myCustomValue {get; set;}
    
    private Lead currentLead;

    public EditLeadCtrExt(ApexPages.StandardController controller) {
        
        this.currentLead = (Lead) controller.getRecord();
        this.currentLead.FirstName = 'Change me';
    }
    

    
    public PageReference myCustomSave(){
    
    if(string.isBlank(currentLead.FirstName)){
    
        currentLead.FirstName.addError('Error1');
        
        ApexPages.Message myMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error2');
        ApexPages.addMessage(myMessage);
        
        myMessage = new ApexPages.Message(ApexPages.Severity.WARNING, 'WARNING');
        ApexPages.addMessage(myMessage);
        
    return null;
    }else{
    currentLead.FirstName += ' ' + myCustomValue;
    insert currentLead;
    return new PageReference('/' + currentLead.Id);
    }
    }
    
    /*
    public string getMyCustomValue(){
        return myCustomValue;
    }
    
    public void setMyCustomValue(string newValue){
        myCustomValue = newValue;
    }*/
}