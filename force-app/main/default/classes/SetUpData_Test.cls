@isTest
public class SetUpData_Test {

   public static Account getPositiveSingleAccount(){
       
                        Account acct = new Account(Name='Test',
                                   Phone='+123',
                                   Website='Test.com',                
                                   Fax='+1234',
                                   BillingStreet='Maple',
                                   BillingCity='Park',
                                   BillingState='Cali',
                                   BillingPostalCode='1234',
                                   BillingCountry='United States of America');
        
        return(acct);
 
        
    }
    
   public static Account getNegativeSingleAccount(){
       
                        Account acct = new Account(Name='Test2',
                                   Phone='+6',
                                   Fax='+6',
                                   BillingStreet='Maple',
                                   BillingCity='Park',
                                   BillingState='Cali',
                                   BillingPostalCode='1234',
                                   BillingCountry='United States');
        
        return(acct);
}

   public static Contact getSingleContact(){ 
       
        Account act = [SELECT Id FROM Account];
  
        Contact con = new Contact(LastName='Marasigan',AccountId=act.Id, Primary__c=TRUE);

        return(con);
               
    }
    
   public static Contact getPositiveSingleContact(){
        
        Account act = [SELECT Id FROM Account];
        Contact con = new Contact(LastName='Mara',AccountId=act.Id, Primary__c=FALSE);
        
        return(con);
    }
    
   public static Contact getNegativeSingleContact(){
        Account act = [SELECT Id FROM Account];
        Contact con = new Contact(LastName='Maras',AccountId=act.Id, Primary__c=TRUE);
        
        return(con);
        
    }
    
   public static List<Contact> getBulkContact(){
            
        Account act = [SELECT Id FROM Account];
        List<Contact> con = new List<Contact>();
            
        for(Integer i=0;i<100;i++) {
            
                Contact cont = new Contact(LastName='Maras' + i ,AccountId=act.Id,Primary__c = FALSE);
                con.add(cont);
        }
        return (con);
            
    }

   public static List<Contact> getNegativeBulkContact(){
            
        Account act = [SELECT Id FROM Account];
        List<Contact> con = new List<Contact>();
            
        for(Integer i=0;i<100;i++) {
            
                Contact cont = new Contact(LastName='Maras' + i ,AccountId=act.Id,Primary__c = TRUE);
                con.add(cont);
        }
        return (con);
            
    }
    
   public static List<Account> getPositiveBulkAccount(){
       
           List<Account> act = new List<Account>();
           for(Integer i=0;i<100;i++) {
					if(i>1 && i<34){
               			Account acct = new Account(Name='Test1' + i, Phone='+123' +i, BillingCountry='United States of America'+i);
    					act.add(acct);
							}
               
					else if(i>35 && i<69){
						Account acct = new Account(Name='Test2' + i, Fax='+147'+i, BillingCountry='United States'+i);
   						 act.add(acct);
										 }
               
					else if(i>70&&i<100){
						Account acct = new Account(Name='Test3' + i, BillingCountry='USA'+i);
 					   act.add(acct);
										}
            
           }
        
        return(act);
 
    }
    
   public static List<Account> getNegativeBulkAccount(){
          List<Account> act = new List<Account>();
           for(Integer i=0;i<100;i++) {
                                       Account acct = new Account(Name='Sample',
                                   	   Phone='+6',
                                   	   Fax='+6',
                                   	   BillingCountry='United States');
               act.add(acct);
           }
        return(act);
    }
    
      /* public static Opportunity getOpportunity(){
           
           Account act = [SELECT Id FROM Account WHERE Name ='Test'];
           
       
           Opportunity Oppo = new Opportunity(Name='Test',
                                              AccountId=act.Id,
                                              CloseDate=system.today(),
                                              StageName ='Prospecting');
        
        return(Oppo);
 
        
    }*/
    
    /*public static  getjson(){
    	
    	ListCustomSetting__c sList = new ListCustomSetting__c();
		sList.Name = 'JSONEndPoint';
		sList.Accept__c = 'application/json';
		sList.Content_Type__c = 'application/json; charset=UTF-8';
		sList.EndPoint__c ='https://maps.googleapis.com/maps/api/geocode/json?address=139+Valero,+Makati,+1227+Kalakhang+Maynila&sensor=false';
		
		HierarchyCustomSetting__c sCus = new HierarchyCustomSetting__c();
		sCus.UseJSON__c = true;
		
		insert sList;
		//return(sCus);
    }*/
    
    
}