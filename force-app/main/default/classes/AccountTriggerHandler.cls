public class AccountTriggerHandler {
    public static void doBeforeUpdate(List<Account> newRec, Map<Id, Account> oldA, Map<Id, Account> newA)
    {
        Set<Id> accountIds = new Set<Id>();

       Account old;
        
       for (account newAcct : newRec) {
             old = oldA.get(newAcct.Id);
            if (newAcct.BillingStreet != old.BillingStreet ||
                newAcct.BillingCity !=old.BillingCity ||
                newAcct.BillingState !=old.BillingState ||
                newAcct.BillingCountry != old.BillingCountry)
            {
                accountIds.add(newAcct.Id);
            }
        }
        if (accountIds.size() > 0)
        {
            contact[] updates = [SELECT Id, accountId
                                 FROM contact
                                 WHERE accountId in :accountIds];
            account newVal;
            for (contact con : updates)
            {
                newVal = newA.get(con.accountId);
                con.MailingStreet = newVal.BillingStreet;
                con.Mailingcity = newVal.BillingCity;
                con.Mailingstate = newVal.BillingState;
                con.Mailingpostalcode = newVal.BillingPostalCode;
                con.Mailingcountry = newVal.BillingCountry;
            }
            Update updates;
        }
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
  public static void doBeforeInsert(List<Account>NewRecords)
   {
         for (Account AccRec: NewRecords){
            if((AccRec.BillingCountry == 'USA') ||
            (AccRec.BillingCountry == 'United States Of America') ||
            (AccRec.BillingCountry == 'United States'))
            {
                AccRec.BillingCountry = 'US';
                if(String.isNotBlank(AccRec.Phone))
                {
                    if (AccRec.Phone.substring(0,2) != '+1')
                    {
                        AccRec.Phone.adderror('US telephone numbers must start with +1.');
                    }
                }
    
              if(String.isNotBlank(AccRec.Fax))
                {
                     if (AccRec.Fax.substring(0,2) !='+1')
                     {
                        AccRec.Fax.adderror('US telephone numbers must start with +1.');
                     }
                }            
           }
            if((AccRec.ShippingCountry == 'USA') ||
            (AccRec.ShippingCountry == 'United States Of America') ||
            (AccRec.ShippingCountry == 'United States'))
            {
                AccRec.ShippingCountry = 'US';        
           }
            if((String.isBlank(AccRec.ShippingStreet)) &&
            (String.isBlank(AccRec.ShippingCity)) &&
            (String.isBlank(AccRec.ShippingState)) &&
            (String.isBlank(AccRec.ShippingPostalCode)) &&
            (String.isBlank(AccRec.ShippingCountry)))  
           {
                AccRec.ShippingStreet = AccRec.BillingStreet;
                AccRec.ShippingCity = AccRec.BillingCity;
                AccRec.ShippingState = AccRec.BillingState;
                AccRec.ShippingPostalCode = AccRec.BillingPostalCode;
                AccRec.ShippingCountry = AccRec.BillingCountry;
            }
       }
    }
}