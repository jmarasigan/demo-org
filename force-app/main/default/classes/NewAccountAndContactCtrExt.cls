public class NewAccountAndContactCtrExt {

    public Boolean freegift {get; set;}
    public Account acct {get; set;}
    public Contact cont {get; set;}
    public User us {get; set;}
    public Contract con{get; set;}
    public Product2 prod {get; set;}
    public Order ord {get; set;}
    public PricebookEntry pri {get; set;}
    
   

    public NewAccountAndContactCtrExt(ApexPages.StandardController controller) {
    
      acct = new Account();
      cont = new Contact();
      pri = new PricebookEntry ();
      ord = new Order();
      prod = new Product2();
      con = new Contract();
        
      
      us = new User();
      us = [SELECT Id,Name FROM User WHERE Id=:userinfo.getuserid()];
        
    }
    
    public PageReference save(){
                                      

            if(string.isBlank(pri.Product2Id) && freegift==true){
                  ApexPages.Message myMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'No product specified');
                  ApexPages.addMessage(myMessage);
                  
                return null;
            
            }
            
            else if(freegift == false && string.isNotBlank(pri.Product2Id)){
                insert acct;
                cont.AccountId = acct.Id;
                cont.Primary__c=true;
                cont.MailingStreet= acct.BillingStreet;
                cont.MailingCity = acct.BillingCity;
                cont.MailingState = acct.BillingState;
                cont.MailingPostalCode = acct.BillingPostalCode;
                cont.MailingCountry = acct.BillingCountry;
                insert cont;
            
            }
            
            else if(freegift == true){
                    
               
                                        
                PricebookEntry price = [SELECT Id, UnitPrice, Product2.Quantity_In_Stock__c,Product2Id, Pricebook2Id
                                        FROM PricebookEntry
                                        WHERE (Product2Id = :pri.Product2Id)
                                        LIMIT 1];
                                        
            
                 if(price.UnitPrice >= 100){
                
                  ApexPages.Message myMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Product. A free gift may only be less than $100');
                  ApexPages.addMessage(myMessage);
                
                  return null;
                
                }else if(price.Product2.Quantity_In_Stock__c == 0){
                
                  ApexPages.Message myMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Product. This product is out of stock');
                  ApexPages.addMessage(myMessage);
                
                  return null;
    
                
                } else{
                
                
                            insert acct;
                            cont.AccountId = acct.Id;
                            cont.Primary__c=true;
                            cont.MailingStreet= acct.BillingStreet;
                            cont.MailingCity = acct.BillingCity;
                            cont.MailingState = acct.BillingState;
                            cont.MailingPostalCode = acct.BillingPostalCode;
                            cont.MailingCountry = acct.BillingCountry;
                            insert cont;
                            
                            
                            
                            Contract newcon = new Contract();
                            
                            newcon.AccountID=acct.Id;
                            newcon.Status='Draft';
                            newcon.StartDate=System.today();
                            newcon.ContractTerm=6;
                            insert newcon;
                            
                            Contract updatecon = new Contract ();
                                        
                            List<Contract> updatelist = [SELECT Id,AccountId, Status
                                                         FROM Contract 
                                                         WHERE (Id=:newcon.Id AND Status='Draft')];                                             
                                                                  
                            updatecon = updatelist.get(0);
                            updatecon.Status='Activated';
                            update updatecon;                                                                               
                            
                            Order newOrd = new Order();
                            
                            newOrd.contractId = newcon.Id;
                            newOrd.AccountId = acct.Id;
                            newOrd.EffectiveDate =System.today();
                            newOrd.Status ='Draft';
                            newOrd.PriceBook2Id = price.PriceBook2Id;
                            
                            insert newOrd;
                            
                            OrderItem ordItem = new OrderItem();
                            
                                         ordItem.OrderId = newOrd.Id;
                                         ordItem.PricebookEntryId = price.Id;
                                         ordItem.UnitPrice = price.UnitPrice;  
                                         ordItem.quantity = 1;  
                                         
                                insert ordItem;
                            
                            Order updateorder = new Order();
                                        
                            List<Order> updateordlist = [SELECT Id,OrderNumber, Status
                                                         FROM Order
                                                         WHERE (Id=:newOrd.Id AND Status='Draft')];
                                        
                                        
                                        updateorder = updateordlist.get(0);
                                        updateorder.Status='Activated';
                                        update updateorder;
                     
                }
            
            }else{
                insert acct;
                cont.AccountId = acct.Id;
                cont.Primary__c=true;
                cont.MailingStreet= acct.BillingStreet;
                cont.MailingCity = acct.BillingCity;
                cont.MailingState = acct.BillingState;
                cont.MailingPostalCode = acct.BillingPostalCode;
                cont.MailingCountry = acct.BillingCountry;
                insert cont;
            
            
            }    
 
    return new PageReference ('/' + acct.Id);    
    
    }
    
    public PageReference cancel(){
    	
    	if(Test.isRunningTest()){
    		
    		return new PageReference ('https://ap5.salesforce.com/001/o');
    		
    	}else{
    		 return new PageReference (ApexPages.currentPage().getParameters().get('retURL'));
    		
    	}
    
       
        

    }
    

}