@IsTest
global class CalculatorServicesMock_Test implements WebServiceMock {

    
    public enum Operations{Add, Subtract, Multiply, Divide}
    
    public Operations currentOp;
    
    public CalculatorServicesMock_Test (Operations ops){
        
        this.currentOp = ops;
    }
    
    public Void doInvoke(Object stub, Object soapRequest, Map<String,Object> responseMap,
                         String endpoint, String soapAction, String requestName,
                         String responseNamespace, String responseName, String responseType){
                             
                             if(currentOp == Operations.Add){
                                 CalculatorServices.doAdd addArgs = ((CalculatorServices.doAdd) soapRequest);
                                 CalculatorServices.doAddResponse addResp = new CalculatorServices.doAddResponse();
                                 addResp.return_x = addArgs.arg0 + addArgs.arg1;
                                 responseMap.put('response_x', addResp);
                             }     
                                 else if(currentOp == Operations.Subtract){
                                 CalculatorServices.doSubtract addArgs = ((CalculatorServices.doSubtract) soapRequest);
                                 CalculatorServices.doSubtractResponse addResp = new CalculatorServices.doSubtractResponse();
                                 addResp.return_x = addArgs.arg0 - addArgs.arg1;
                                 responseMap.put('response_x', addResp);
                             }          
                                 else if(currentOp == Operations.Multiply){
                                 CalculatorServices.doMultiply addArgs = ((CalculatorServices.doMultiply) soapRequest);
                                 CalculatorServices.doMultiplyResponse addResp = new CalculatorServices.doMultiplyResponse();
                                 addResp.return_x = addArgs.arg0 * addArgs.arg1;
                                 responseMap.put('response_x', addResp);
                             } 
                                 else if(currentOp == Operations.Divide){
                                 CalculatorServices.doDivide addArgs = ((CalculatorServices.doDivide) soapRequest);
                                 CalculatorServices.doDivideResponse addResp = new CalculatorServices.doDivideResponse();
                                 addResp.return_x = addArgs.arg0 / addArgs.arg1;
                                 responseMap.put('response_x', addResp);
                             }                    
                             
     }
    
}