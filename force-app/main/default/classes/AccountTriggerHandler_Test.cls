@isTest
public class AccountTriggerHandler_Test {
    
    @isTest static void accountPositiveInsert(){
        
        
        Test.startTest();
        insert SetUpData_Test.getPositiveSingleAccount();
        Test.stopTest();
        List <Account> acc = [SELECT Id, Name FROM Account WHERE (Name='Test')];
        
        for(Account acc2: acc){
            System.assertEquals(acc2.Name, 'Test');
        }
    }
    
    
    @isTest static void accountPositiveUpdate(){
        
        insert SetUpData_Test.getPositiveSingleAccount();
        insert SetUpData_Test.getSingleContact();
        Account acc = [SELECT Id FROM Account WHERE Name='Test'];
        acc.BillingCountry='United States';
        Test.startTest();
        update acc;
        Test.stopTest();
        
        List<Contact> sample = [SELECT Id,MailingCountry FROM Contact WHERE (Id=:acc.Id)];
        for(Contact sample2:sample){
            System.assertEquals(sample2.MailingCountry, 'United States');
        }
    }
    
    @isTest static void accountPositiveBulkInsert(){
        
        Test.startTest();
        try{
            insert SetUpData_Test.getPositiveBulkAccount();
            
            Test.stopTest();
        }
        catch(DmlException e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('US telephone numbers must start with +1');
            system.assertEquals(expectedExceptionThrown,true);
        }
        
    }
    
    @isTest static void accountNegativeInsert(){
        
        
        Test.startTest();
        try{
            insert SetUpData_Test.getNegativeSingleAccount();
            
            Test.stopTest();
        }
        catch(DmlException e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('US telephone numbers must start with +1');
            system.assertEquals(expectedExceptionThrown,true);
        }
        
        
    }
    
    @isTest static void accountBulkNegativeInsert(){
        
        
        Test.startTest();
        try{
            insert SetUpData_Test.getNegativeBulkAccount();
            
            
            Test.stopTest();
        }
        catch(DmlException e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('US telephone numbers must start with +1');
            system.assertEquals(expectedExceptionThrown,true);
        }
        
    }
    
    
    
}