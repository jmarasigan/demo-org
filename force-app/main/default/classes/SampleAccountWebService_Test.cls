@IsTest
private class SampleAccountWebService_Test {
    
    @testSetup static void setupTestData(){
    	
    	
    	insert SetUpData_Test.getPositiveSingleAccount();
    }
    
    @isTest static void getAccount(){
    	
    	List<Account> accts = [SELECT Id FROM Account WHERE (Name ='Test')];
    	
    	String resultName;
    	
    	Test.startTest();
    	resultName = SampleAccountWebService.getAccountName(accts.get(0).Id);
    	Test.stopTest();
    	
    	System.assertEquals('Test', resultName);
    }
    
     @isTest static void getInvalidAccount(){

       String resultName;
        
       Test.startTest();
        resultName = SampleAccountWebService.getAccountName('me!');
        Test.stoptest();
        
       System.assertEquals('INVALID ID',resultName);
        
   }     	
       @isTest static void getFullyAccountDetailsOneFilter(){
        
       List<SampleAccountWebService.AccountDetails> resultRecs;
       
        SampleAccountWebService.AccountQueryOptions  qo = new SampleAccountWebService.AccountQueryOptions();
        qo.name='Test';
        
       Test.startTest();
       
       resultRecs = SampleAccountWebService.getAccountDetails(qo);
       
       Test.stoptest();
        
        for(SampleAccountWebService.AccountDetails resRec: resultRecs){
        
        System.assert(resRec.name.contains('Test'));
        
        }
          }
          
          @isTest static void getFullAccountMultiFilter() {
        
       List<SampleAccountWebService.AccountDetails> resultRecs;
        SampleAccountWebService.AccountQueryOptions qo = new SampleAccountwebService.AccountQueryOptions();
        qo.name = 'Test';
        qo.website = 'Test';
        qo.billCountry = 'USA';
        
       
       Test.startTest();
        resultRecs = SampleAccountWebService.getAccountDetails(qo);
        Test.stopTest();
        
       for(SampleAccountWebService.AccountDetails resRec : resultRecs){
            System.assert(resRec.name.contains('Test Account'));
            System.assert(resRec.website.contains('Test Account'));
            System.assert(resRec.billingCountry.contains('Test Account'));
        	}
         }
         
         @isTest static void getFullAccountNoMatch() {
        
       List<SampleAccountWebService.AccountDetails> resultRecs;
        SampleAccountWebService.AccountQueryOptions qo = new SampleAccountwebService.AccountQueryOptions();
        qo.name = 'Testo';
        qo.website = 'Testo';
        qo.billCountry = 'PH';
        
       
       Test.startTest();
        resultRecs = SampleAccountWebService.getAccountDetails(qo);
        Test.stopTest();
  
           System.assert(resultRecs.isEmpty());                          
   }
    
  /* @isTest static void getFullAccountDetailsNullInput() {
        
       List<SampleAccountWebService.AccountDetails> resultRecs;
        Test.startTest();
        try{
            resultRecs = SampleAccountWebService.getAccountDetails(null);
        }catch (AssertException ae){
            System.assert(ae.getMessage);
        }
        
 */
                                     
   
}