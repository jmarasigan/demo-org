@RestResource(urlMapping='/SampleRestWS/*')
global class SampleRESTWebService {
	
	global class NewAccountInfo{
		
		String acctName;
		String webs;
		String acctId;
	}
	
	global class UpdateAccountInfo{
		String acctName;
		String webs;
		Datetime createdDate;
		String createdBy;
		Integer position;
		List<Contact> acctContacts;
	}
    
    @HttpGet
    global static Account doGet(){
    	
    	RestRequest req = RestContext.request;
    	Id acctId = req.requestURI.substring(req.requestURI.lastIndexof('/') + 1);
    	
    	List<Account> accList = [SELECT Id, Name FROM Account WHERE (Id=:acctId)];
    	
    	return accList.get(0);
    }
    
    @HttpPost
    global static Account doPost(NewAccountInfo newAcct){
    	
    	Account acct = new Account(Name=newAcct.acctName, BillingCountry = 'US', Website=newAcct.webs);
    	
    	insert acct;
    	
    	return acct;
    }
    
    @HttpPut
    global static string doPut(){
    	
    	RestRequest req = RestContext.request;
    	String reqData = req.requestBody.toString();
    	
    	System.debug('reqData : ' + reqData);
    	
    	return reqData;
    	
    /*	List<Account> accList = [SELECT Id, Name, Website, CreatedDate, CreatedBy.Name,
    	(SELECT Id, Name FROM Contacts) FROM Account WHERE (Id=:editAcct.acctId)];
    	
    	Account acct = accList.get(0);
    	acct.Name = editAcct.acctName;
    	acct.Website = editAcct.webs; 
    	update acct;
    	
    	UpdateAccountInfo upAcct = new UpdateAccountInfo();
    	upAcct.acctName = acct.Name;
    	upAcct.webs = acct.Website;
    	upAcct.createdDate = acct.CreatedDate;
    	upAcct.createdBy = acct.CreatedBy.Name;
    	
    	List<AggregateResult> acctCounter = [SELECT COUNT(Id) numAccounts FROM Account];
    	
    	
    	for(AggregateResult ar: acctCounter){
    		
    		upAcct.position = Integer.valueOf(ar.get('numAccounts'));
    		
    	}
    	
    	upAcct.acctContacts = acct.Contacts;
    	
    	return upAcct;*/
    	
    	
    }
    
    @HttpDelete
    global static String doDelete(){
    	
    	RestRequest req = RestContext.request;
    	
    	if (req.params !=null){
    	string msg = '';	
    		List<String> oc = new List<String>();
    		oc.addAll(req.params.keyset());
    		oc.sort();
    		for (string par: oc){ //req.params.keyset()
    			msg+= req.params.get(par)+' ';
    			
    		}
    		
    		return msg.trim();
    	}
    	
    	
    	/*Id acctId = req.params.get('delId');
    	if(req.params !=null){
    		
    		if(acctId != null){
    			List<Account> accList = [SELECT Id, Name FROM Account WHERE (Id=:acctId)];
    			
    			delete accList.get(0);
    			
    			return 'Delete Successfully';
    		}else{
    			return'No Deletion parameter specified';
    		}
    	}*/else{
    		return'No parameter specified';
    	}
    }
}