public with sharing class ViewRecordListingCtrl {

    public static final string  SEL_OBJ_ACCOUNT = 'Account';
    public static final string  SEL_OBJ_CONTACT = 'Contact';
    public static final string  SEL_OBJ_OPPORTUNITY = 'Opportunities';
    
    //public static final Integer MAX_PAGE_SIZE = 5;
        
    public List<Account> acctList {get; set;}
    public List<Contact> contList {get; set;}
    public List<Opportunity> oppList {get; set;}
    
    public Boolean renAcc {get; set;}
    public Boolean renCont {get; set;}
    public Boolean renOpp {get; set;}
    
    public String selectedObj {get; set;}
    public Integer MAX_PAGE_SIZE  {get; set;}
    public Integer currentPages {get; set;}
    public Integer totalPages {get; set;}
    
    public ApexPages.StandardSetController recordSet;
    
    public ViewRecordListingCtrl(){
    
        MAX_PAGE_SIZE = 5;
        this.selectedObj = SEL_OBJ_ACCOUNT;
        //recordSet.setPageSize(5);
       // showRecords();

    } 
    
    
    public void getRecords(){
    
    
    
        if(selectedObj.equals(SEL_OBJ_ACCOUNT)){
        
            recordSet = new ApexPages.StandardSetController([SELECT Id, Name, Phone, Website
                                                             FROM Account
                                                             ORDER BY Name ASC]);
                                                             
                                                             renAcc = true;
                                                             renCont = false;
                                                             renOpp = false;
                                                             
                                                             
        
        }
         else if(selectedObj.equals(SEL_OBJ_CONTACT)){
         
             recordSet = new ApexPages.StandardSetController([SELECT Id, Name, Email, Primary__c
                                                             FROM Contact
                                                             ORDER BY Name ASC]);
                                                             
                                                             
                                                             renAcc = false;
                                                             renCont = true;
                                                             renOpp = false;
        
        
        }
        else if(selectedObj.equals(SEL_OBJ_OPPORTUNITY)){
        
                        recordSet = new ApexPages.StandardSetController([SELECT Id, Name, CloseDate, AccountId
                                                             FROM Opportunity
                                                             ORDER BY Name ASC]);
                                                             
                                                             renAcc = false;
                                                             renCont = false;
                                                             renOpp = true;
        
        }
        
        
       // recordSet.setPageSize(selectNum);
       if(MAX_PAGE_SIZE == 5)
       {
        recordSet.setPageSize(MAX_PAGE_SIZE);
        }
        else if(MAX_PAGE_SIZE == 10){
        recordSet.setPageSize(10);
        }
        else if(MAX_PAGE_SIZE == 15){
        recordSet.setPageSize(15);
        }
        else if(MAX_PAGE_SIZE == 20){
        recordSet.setPageSize(20);
        }
        else if(MAX_PAGE_SIZE == 25){
        recordSet.setPageSize(25);
        }
    }
    

    
    public void viewRecords(){
    
        if(selectedObj.equals(SEL_OBJ_ACCOUNT)){
                acctList = recordSet.getRecords();
        
        }
        else if(selectedObj.equals(SEL_OBJ_CONTACT)){
            contList = recordSet.getRecords();
        
        
        }
        else if(selectedObj.equals(SEL_OBJ_OPPORTUNITY)){
            oppList = recordSet.getRecords();
        
        
        }
        
        currentPages = recordSet.getPageNumber();
        totalPages = (Integer)Math.ceil((Decimal)recordset.getResultSize() / MAX_PAGE_SIZE);
    
    }
    
    public void showRecords(){
    
        getRecords();
        viewRecords();
    }
    
    public void goToNext(){
    
        if(recordSet.getHasNext()){
        
               recordSet.next();
               viewRecords();
        
        }
    }
    
        public void goToPrevious(){
    
        if(recordSet.getHasPrevious()){
        
               recordSet.previous();
               viewRecords();
        
        }
    }
    
        public void goToFirst(){
    
        if(recordSet.getHasNext()){
        
               recordSet.first();
               viewRecords();
        
        }    }
    
        public void goToLast(){
    
        if(recordSet.getHasPrevious()){
        
               recordSet.last();
               viewRecords();
        
        }
    }
    

    

        
    
}