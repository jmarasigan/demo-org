@RestResource(urlMapping='/JMarasigan/*')

global class  UniversalOnlineShopREST {
    
global class Productdetails{
		
		 string prodId;
    	 string name;
   		 string cat;
    	 string code;
		 decimal price;
    	 decimal stock;
	}
	
global class OrderResponseDetails{
        
         string orderNumber;
  public string message;  
         string status;
        
    }
    
global class AccountServiceDetails{
         string AccountId;
    }
    
global class inputOrderRequestDetails{
    	
        public string productCode2;
        public decimal quantity;
        
    }
     
    
    @HttpGet
    global static List<Productdetails> getProductItems(){
    	
    	List<PricebookEntry> priceList = [SELECT Product2.Name, Id,Product2.ProductCode,Product2.Quantity_In_Stock__c,UnitPrice
    	 								  FROM PricebookEntry
    	  								  WHERE (IsActive=TRUE)];
    	  								  
    	List <Productdetails> priList = new List<Productdetails>();
    	
    	for(PricebookEntry pri: priceList){
    		Productdetails pric = new Productdetails();
    		pric.Name=pri.Product2.Name;
    		pric.code=pri.Product2.ProductCode;
    		pric.price=pri.UnitPrice;
    		pric.Stock = pri.Product2.Quantity_In_Stock__c;
    		priList.add(pric);
    	}

    	return priList;
    	
    	
    }
    
		@HttpPost    
        global static List<OrderResponseDetails> getsubmitOrder(string accId, List<inputOrderRequestDetails> ordr){
    	
    	List<OrderResponseDetails> message = new List<OrderResponseDetails>();
    	
    	String queryStr ='SELECT Id, AccountId FROM Contract WHERE Status =\'Activated\' AND AccountId =:accId LIMIT 1';
    	
    	Savepoint sp = Database.setSavepoint();
    	
    	List<Contract> cont =  Database.query(queryStr);
    	
    			if(cont.isEmpty()){
					
					OrderResponseDetails reply = new OrderResponseDetails();
					
					    reply.orderNumber = '';
                        reply.message ='Invalid Account Specified';
                        reply.status ='Failed';               
                        message.add(reply);
                        Database.rollback(sp);
                        
				}else{
				
				
				
        Contract con = cont.get(0);
        
            Order ord = new Order();
            
            ord.contractId =con.Id;
            ord.AccountId = con.AccountId;
            ord.EffectiveDate =System.today();
            ord.Status ='Draft';
                        
            insert ord;
            
            String queryPriceBookEntry ='SELECT ID,ProductCode,UnitPrice FROM PricebookEntry';
            
            List<PriceBookEntry> price =Database.query(queryPriceBookEntry);
            
            Map<String,PricebookEntry> pricemap = new  Map<String,PricebookEntry>();
            

                         
             for(PricebookEntry priceb: price){
                
                pricemap.put(priceb.ProductCode,priceb);
             }
             
     
             
              List<OrderItem> ordItem = new List<OrderItem>();
             
                    for( inputOrderRequestDetails inputOR: ordr){
                
              
                     OrderItem orItem = new OrderItem();
                     orItem.OrderId = ord.Id;  
					 orItem.quantity = inputOR.Quantity; 

                     
                     if(pricemap.containsKey(inputOR.productCode2)){
                        
                 		String quan = String.valueOf(orItem.quantity);
                    
                    		if(orItem.quantity <= 0 || (!quan.contains('.00') && quan.contains('.')) ){
                           		OrderResponseDetails reply = new OrderResponseDetails();
              
                          		 reply.orderNumber = '';    
                          		 reply.message ='Invalid Quantity Specified';
                           		 reply.status ='Failed';              
                           		 message.add(reply);
								 Database.rollback(sp);
                           		 
                         	} else{
                         		orItem.PriceBookEntryId =pricemap.get(inputOR.productCode2).Id;
                    			orItem.unitPrice = pricemap.get(inputOR.productCode2).UnitPrice; 
                         		                    
                     			ordItem.add(orItem);
                     			 
                         	}
                         	
                    } else{
                    	
                    	OrderResponseDetails reply = new OrderResponseDetails();
					
					    reply.orderNumber = '';
                        reply.message ='Invalid Product Code Specified';
                        reply.status ='Failed';               
                        message.add(reply);
						Database.rollback(sp);
                    }
  
                    }
                    
                   
                   
                    

                 if(!ordItem.isEmpty() && message.isEmpty()){
                   		
                   insert ordItem;
                   			 	
                   			 	
                   	Order updateord = new Order();
                    
                    List<Order> updatelist = [SELECT Id,OrderNumber, Status
                     						  FROM Order
                     						  WHERE (Id=:ord.Id AND Status='Draft')];
                    
                    updateord = updatelist.get(0);
                    updateord.Status='Activated';
       				update updateord;
                   
					Boolean status = false;
					
                    	for(OrderItem orderedItem :ordItem){
                    		
                    		if(status == false){
                     		   OrderResponseDetails reply = new OrderResponseDetails();
              
                       		   reply.orderNumber = updateord.OrderNumber;
                       		   reply.message ='Order saved sucessfully';
                       		   reply.status ='Success';               
                        	   message.add(reply);
                        	   
                        	   status=true;
                    			}

         
        } 
       }
      }	

            return message;
				 
    }	
    
}