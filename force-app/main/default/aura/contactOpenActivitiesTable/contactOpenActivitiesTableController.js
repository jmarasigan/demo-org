({
    gotoRecord : function(component, event, helper) {
        
        var recId = event.getSource().get("sfdc-activity-record-id");
        
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": "recId",
        });
        navEvt.fire();
    }
})