({
    //shows the modal
    openModal : function(component,event,helper){
        /*  component.set("v.modalIsOpen", true);
        var canvasModal = document.querySelector(".canvas_modal");
        
        if(canvasModal){
            canvasModal.style.display = "block";
        }*/
        document.getElementById("modal").style.display = "block";
        /*var canvas = document.getElementById('can');
        alert(canvas);*/
        
        var canvas, ctx, flag = false,
            prevX = 0,
            currX = 0,
            prevY = 0,
            currY = 0,
            dot_flag = false;
        
        var w,
            h;
        
        
        var x = "black",
            y = 2;
        
        function init() {
            canvas = document.getElementById('can');
            ctx = canvas.getContext('2d');
            ctx.fillStyle = "white";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            w = canvas.width;
            h = canvas.height;
            
            canvas.addEventListener("mousemove", function (e) {
                findxy('move', e)
            }, false);
            canvas.addEventListener("mousedown", function (e) {
                findxy('down', e)
            }, false);
            canvas.addEventListener("mouseup", function (e) {
                findxy('up', e)
            }, false);
            canvas.addEventListener("mouseout", function (e) {
                findxy('out', e)
            }, false);
            
            canvas.addEventListener('touchstart', function (t) {
                findxytouch('down', t);
            },false);
            canvas.addEventListener('touchmove', function (t) {
                findxytouch('move', t);
            },false);
            canvas.addEventListener('touchend', function (t) {
                findxytouch('up', t);
            },false);
            canvas.addEventListener('touchout', function (t) {
                findxytouch('out', t);
            },false);
            
        }
        function draw() {
            ctx.beginPath();
            ctx.moveTo(prevX, prevY);
            ctx.lineTo(currX, currY);
            ctx.strokeStyle = x;
            ctx.lineWidth = y;
            ctx.stroke();
            ctx.closePath();
        }
        
        function findxytouch(touch, t) {
            if (touch == 'down') {
                prevX = currX;
                prevY = currY;
                currX = e.touches[0].clientX - canvas.getBoundingClientRect().left;
                currY = e.touches[0].clientY - canvas.getBoundingClientRect().top;
                
                flag = true;
                dot_flag = true;
                if (dot_flag) {
                    ctx.beginPath();
                    ctx.fillStyle = x;
                    ctx.fillRect(currX, currY, 2, 2);
                    ctx.closePath();
                    dot_flag = false;
                }
            }
            if (touch == 'up' || touch == "out") {
                flag = false;
            }
            if (touch == 'move') {
                if (flag) {
                    prevX = currX;
                    prevY = currY;
                    currX = e.touches[0].clientX - canvas.getBoundingClientRect().left;
                    currY = e.touches[0].clientY - canvas.getBoundingClientRect().top;
                    draw();
                }
            }
        }
        
        function findxy(res, e) {
            if (res == 'down') {
                prevX = currX;
                prevY = currY;
                currX = e.clientX - canvas.getBoundingClientRect().left;
                currY = e.clientY - canvas.getBoundingClientRect().top;
                
                flag = true;
                dot_flag = true;
                if (dot_flag) {
                    ctx.beginPath();
                    ctx.fillStyle = x;
                    ctx.fillRect(currX, currY, 2, 2);
                    ctx.closePath();
                    dot_flag = false;
                }
            }
            if (res == 'up' || res == "out") {
                flag = false;
            }
            if (res == 'move') {
                if (flag) {
                    prevX = currX;
                    prevY = currY;
                    currX = e.clientX - canvas.getBoundingClientRect().left;
                    currY = e.clientY - canvas.getBoundingClientRect().top;
                    draw();
                }
            }
        }
        
        init();
        
    },
    
    closeModal : function(component,event,helper){
        /* component.set("v.modalIsOpen", false);
        var canvasModal = document.querySelector(".canvas_modal");
        console.log("canvasModal: " + canvasModal.nodeName );
        canvasModal.style.display = "none";*/
        document.getElementById("modal").style.display = "none";
        /*var canvas = document.querySelector('.canvasBoard');
        var context = canvas.getContext("2d");
        context.clearRect(0, 0, canvas.width, canvas.height);*/
        //document.location.reload();        
    },
    
    clear: function(component, event, helper) {
        var canvas = document.querySelector('.canvasBoard');
        var context = canvas.getContext("2d");
        
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.fillStyle = "white";
        context.fillRect(0, 0, canvas.width, canvas.height);
        var colorSelect = document.querySelector(".active");
        console.log("colorSelect: " + colorSelect.nodeName);
        context.fillStyle = colorSelect.style.backgroundColor;
        context.strokeStyle = colorSelect.style.backgroundColor;
        
    },
    
    save: function(component, event, helper) {
        
        var MAX_FILE_SIZE = 750000;
        
        var canvas = document.querySelector('.mycanvas');
        var context = canvas.getContext("2d");
        /*context.fillStyle = "white";
        context.fillRect(0, 0, canvas.width, canvas.height);*/
        var img = canvas.toDataURL();
        var recid = component.get("v.recordId");
        
        var filename = component.find("filename").get("v.value");
        
        
        var toastEvent = $A.get("e.force:showToast");
        var toastSave = $A.get("e.force:showToast");
        
        function isWhitespaceOrEmpty(text) {
            return /[^\s]/.test(text);
        }
        
        var data = isWhitespaceOrEmpty(filename); 
        
        if(data == false || filename == 'null' || filename == ''){
            
            toastEvent.setParams({
                "title": "No FileName",
                "message": "Please type in a filename" 
            });
            toastEvent.fire();
            
        }
        else if(data == true){
            
            if(canvas.size > MAX_FILE_SIZE){
                alert('File size cannot exceed ' + MAX_FILE_SIZE + ' bytes.\n' + 'Selected file size: ' + canvas.size);
                return size;
            }
            
            
            var action = component.get("c.saveTheFile");
            
            action.setParams({
                
                base64Data:img,
                recId:recid,
                filename:filename
                
            });
            
            action.setCallback(this, function(a){
            });
            
            $A.enqueueAction(action);
            var canvasModal = document.querySelector(".canvas_modal");
            console.log("canvasModal: " + canvasModal.nodeName );
            canvasModal.style.display = "none";
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.fillStyle = "white";
            context.fillRect(0, 0, canvas.width, canvas.height);
            document.getElementById("modal").style.display = "none";
            
            toastSave.setParams({
                "title": "FileSaved",
                "message": "File has been saved" 
            });
            toastSave.fire();
            
            // $A.get('e.force:refreshView').fire();
            document.location.reload(true);
            
        }
        
        
        
    },   
    
})