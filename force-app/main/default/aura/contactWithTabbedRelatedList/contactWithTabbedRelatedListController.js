({
    
    doInit : function(component, event, helper) {
        var contactId = component.get("v.recordId");
        console.log("contactId: " + contactId);
        var action = component.get("c.getContact");
        action.setParams({"recId":contactId});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(component.isValid() && (state === "SUCCESS") && response.getReturnValue() != null){
                component.set("v.currentContact", response.getReturnValue());
                component.set("v.contactOpenActivities", response.getReturnValue().OpenActivities);
            }else{
                console.log("Failed with state: " + state);
                console.log("Returned:");
                console.log(response.getReturnValue());
            }
            
            
        });
        
        $A.enqueueAction(action);
    },
    
    doEdit : function(component, event, helper) {
        
        var editEvent = $A.get("e.force:editRecord");
        editEvent.setParams({"recordId":component.get("v.recordId")});
        editEvent.fire();
    },
    
    doDelete : function(component, event, helper) {
        var contactId = component.get("v.recordId");
        
        var action = component.get("c.deleteContact");
        action.setParams({"recId":contactId});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(component.isValid() && (state === "SUCCESS") && response.getReturnValue() != false){
                
                console.log('returned');
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The record has been deleted successfully."
                });
                toastEvent.fire();  
                
                var homeEvent = $A.get("e.force:navigateToObjectHome");
                homeEvent.setParams({
                    "scope": "Contact"
                });
                homeEvent.fire();
                
                
                
            } else{
                console.log("Failed with state: " + state);
                console.log("Returned:");
                console.log(response.getReturnValue());
            }
        });
        
        $A.enqueueAction(action);       
    }
    
    
})