/* Created by Mark Kevin Mapacpac */
console.log("HELLO UNIVERSE");
var canvas = document.getElementById("canvasBoard");
var context = canvas.getContext("2d");

var dragging, dragStartLocation, snapshot, shape='line' ,color, savePoint;

function init(){


context.lineWidth = 6;
context.lineCap = 'round';
context.clearRect(0, 0, canvas.width, canvas.height);
context.fillStyle = "white";
context.fillRect(0, 0, canvas.width, canvas.height);


var choseColor = document.getElementsByClassName('active')[0];
console.log(choseColor.style.backgroundColor);
context.strokeStyle = choseColor.style.backgroundColor;
context.fillStyle = choseColor.style.backgroundColor;
color = choseColor.style.backgroundColor;

}

function getCanvasCoordinates(event) {
var x = event.clientX - canvas.getBoundingClientRect().left,
	y = event.clientY - canvas.getBoundingClientRect().top;

return {x: x, y: y};
}

function getTouchCanvasCoordinates(event) {
//var touch = event.touches[0];
var x = event.touches[0].clientX - canvas.getBoundingClientRect().left,
	y = event.touches[0].clientY - canvas.getBoundingClientRect().top;
console.log(x);
console.log(y);
return {x: x, y: y};
}

function drawLine(position) {
context.lineWidth=6;
context.fillStyle = color;
context.lineTo(position.x, position.y);
context.stroke();
context.beginPath();
context.arc(position.x, position.y, 2, 0, Math.PI*2);
context.fill();
context.beginPath();
context.moveTo(position.x, position.y);

}

function drawCircle(position) {

var ellCenter_x = dragStartLocation.x - (dragStartLocation.x - position.x)/2;
var ellCenter_y = dragStartLocation.y - (dragStartLocation.y - position.y)/2;
var ellWidth = Math.abs((dragStartLocation.x - position.x)/2);
var ellHeight = Math.abs((dragStartLocation.y - position.y)/2);

context.beginPath();
context.ellipse(ellCenter_x, ellCenter_y, ellWidth , ellHeight,0 * Math.PI/180,0,2 * Math.PI);
context.stroke();
}

function drawRectangle(position){

var centerx = dragStartLocation.x - position.x; //distance between x
var centery = dragStartLocation.y - position.y; //distance between y

context.beginPath();
context.rect(dragStartLocation.x,dragStartLocation.y, centerx*(-1),centery*(-1));
context.stroke();
}

function drawEraser(position){

context.fillStyle = 'white';
context.strokeStyle = 'white';
context.lineWidth=10;
context.lineTo(position.x, position.y);
context.stroke();
context.beginPath();
context.arc(position.x, position.y, 5, 0, Math.PI*2);
context.fill();
context.beginPath();
context.moveTo(position.x, position.y);

}

function clearCanvas(){
takeSavePoint();
context.clearRect(0,0,canvas.width ,canvas.height);
context.fillStyle = "white";
context.fillRect(0, 0, canvas.width, canvas.height);

}

function takeSavePoint() {

savePoint = context.getImageData(0, 0, canvas.width, canvas.height);

}

function restoreSavePoint() {
if(savePoint != null){
   context.putImageData(savePoint, 0, 0);
}
}

function takeSnapshot() {

snapshot = context.getImageData(0, 0, canvas.width, canvas.height);
}

function restoreSnapshot() {
context.putImageData(snapshot, 0, 0);
}


function engage(event){

takeSavePoint();
dragging = true;
dragStartLocation = getCanvasCoordinates(event);
if(shape == 'ellipse' || shape == 'rectangle'){
	takeSnapshot();
	context.beginPath();
}else if (shape == 'line'){
	if(dragging === true){
		context.lineCap= "round";
		drawLine(getCanvasCoordinates(event));
	}
}else if (shape == 'eraser'){
	if(dragging === true){
		
		drawEraser(getCanvasCoordinates(event));
	}
}

    
}

function touchengage(event){

takeSavePoint();
dragging = true;
dragStartLocation = getTouchCanvasCoordinates(event);
if(shape == 'ellipse' || shape == 'rectangle'){
	takeSnapshot();
	context.beginPath();
}else if (shape == 'line'){
	if(dragging === true){
		context.lineCap= "round";
		drawLine(getTouchCanvasCoordinates(event));
	}
}else if (shape == 'eraser'){
	if(dragging === true){
		
		drawEraser(getTouchCanvasCoordinates(event));
	}
}

    event.preventDefault();
}

function disengage(event) {

dragging = false;

var position = getCanvasCoordinates(event);
if(shape=="rectangle" && dragging){
	restoreSnapshot();
	drawRectangle(position);
	context.beginPath();
	
}else if(shape == "ellipse" && dragging){
	restoreSnapshot();
	drawCircle(position);
	context.beginPath();
	
}else if(shape == "line" || shape == 'eraser'){
		context.beginPath();
}
//drawLine(position);
context.beginPath();
}

function touchdisengage(event) {

dragging = false;
//var position = getTouchCanvasCoordinates(event);
//console.log("position " + position);
if(shape=="rectangle" && dragging){
	//var position = getTouchCanvasCoordinates(event);
	dragging = false;
	restoreSnapshot();
	//drawRectangle(position);
	context.beginPath();
	
}else if(shape == "ellipse" && dragging){
	//var position = getTouchCanvasCoordinates(event);
	dragging = false;
	restoreSnapshot();
	//drawCircle(position);
	context.beginPath();
	
}else if(shape == "line" || shape == 'eraser'){
	//var position = getTouchCanvasCoordinates(event);
	dragging = false;
		context.beginPath();
}
//drawLine(position);
context.beginPath();
}

function drag(event) {
var position;
if (dragging === true) {
	
	var position = getCanvasCoordinates(event);
	if(shape=="rectangle"){
		restoreSnapshot();
		drawRectangle(position);
	}else if(shape == "ellipse"){
		restoreSnapshot();
		drawCircle(position);
	}else if(shape == "line"){
		drawLine(position);
	}else if(shape == "eraser"){
		drawEraser(position);
	}
	//drawLine(position);
}
    
}

function touchdrag(event) {
    var position;
    if (dragging === true) {

        var position = getTouchCanvasCoordinates(event);
        if(shape=="rectangle"){
            restoreSnapshot();
            drawRectangle(position);
        }else if(shape == "ellipse"){
            restoreSnapshot();
            drawCircle(position);
        }else if(shape == "line"){
            drawLine(position);
        }else if(shape == "eraser"){
            drawEraser(position);
        }
        //drawLine(position);
    }
    event.preventDefault();
}

function colorPanel(){

var color1 = document.getElementById("color1").value;
var color2 = document.getElementById("color2").value;
var color3 = document.getElementById("color3").value;
	
var panel = document.createElement('div');
panel.className = 'swatch active';
panel.style.background = color1;
panel.addEventListener('click', setColor);
document.getElementById('colors').appendChild(panel);

var panel = document.createElement('div');
panel.className = 'swatch';
panel.style.background = color2;
panel.addEventListener('click', setColor);
document.getElementById('colors').appendChild(panel);


var panel = document.createElement('div');
panel.className = 'swatch';
panel.style.background = color3;
panel.addEventListener('click', setColor);
document.getElementById('colors').appendChild(panel);


}

function setColor(event){
var panelClicked = event.target;
color = panelClicked.style.backgroundColor;
context.fillStyle = color;
context.strokeStyle = color;
var activeColor = document.getElementsByClassName('active')[0];
if(activeColor){
   activeColor.className = 'swatch';
}
panelClicked.className += ' active';

}

function tools(){
    
var tool = document.createElement("lightning:icon");
tool.id = "sampeIcon";
tool.iconName = "utility:comments";
tool.size = "small";
document.getElementById("tools").appendChild(tool);
console.log('tial icon: ' + tool);
    
var tool = document.createElement('lightning:button');
tool.id = "trial";
tool.variant = "brand";
tool.iconName = "utility:download";
tool.label = "trial";
tool.innerHTML = "TRIAL";
tool.title = "trialTittle";
tool.name = "toolName";
tool.className = 'slds-button slds-button_brand';
console.log(tool);
document.getElementById("tools").appendChild(tool);

    
var tool = document.createElement('BUTTON');
tool.value = 'line';
tool.className = 'slds-button slds-button_brand';
tool.innerHTML = "LINE";
//console.log("tool" + tool);
tool.addEventListener('click', selectTool);
document.getElementById('tools').appendChild(tool);

var tool = document.createElement('BUTTON');
tool.value = 'rectangle';
tool.className = 'slds-button slds-button_neutral';
tool.innerHTML = "RECTANGLE";
//console.log("tool" + tool);
tool.addEventListener('click', selectTool);
document.getElementById('tools').appendChild(tool);

var tool = document.createElement('BUTTON');
tool.value = 'ellipse';
tool.className = 'slds-button slds-button_neutral'
tool.innerHTML = "ELLIPSE";
//console.log("tool" + tool);
tool.addEventListener('click', selectTool);
document.getElementById('tools').appendChild(tool);

var tool = document.createElement('BUTTON');
tool.value = 'eraser';
tool.className = 'slds-button slds-button_neutral'
tool.innerHTML = "ERASER";
//console.log("tool" + tool);
tool.addEventListener('click', selectTool);
document.getElementById('tools').appendChild(tool);

var tool = document.createElement('BUTTON');
tool.value = 'undo';
tool.className = 'slds-button slds-button_neutral'
tool.innerHTML = "UNDO";
//console.log("tool" + tool);
tool.addEventListener('click', restoreSavePoint);
document.getElementById('tools').appendChild(tool);


var tool = document.createElement('BUTTON');
tool.value = 'clear';
tool.className = 'slds-button slds-button_neutral'
tool.innerHTML = "CLEAR";
//console.log("tool" + tool);
tool.addEventListener('click', clearCanvas);
document.getElementById('tools').appendChild(tool);

}

function selectTool(event){

context.fillStyle = color;
context.strokeStyle = color;
var toolClicked = event.target;
var toolChsn = toolClicked.value;
if(toolChsn == 'eraser'){
	canvas.className += ' customCursor';
	
}else{
	if(canvas.classList.contains("customCursor")){
		canvas.classList.remove("customCursor");
	}
	
}
shape = toolChsn;
var active = document.getElementsByClassName('slds-button_brand')[0];
if(active){
	active.className = 'slds-button slds-button_neutral';
}
toolClicked.classList.remove("slds-button_neutral");
toolClicked.className += ' slds-button_brand';


}

tools();
colorPanel();
init();

canvas.addEventListener('touchstart', touchengage);
canvas.addEventListener('touchmove', touchdrag);
canvas.addEventListener('touchend', touchdisengage);


canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mousemove', drag);
canvas.addEventListener('mouseup', disengage);
canvas.addEventListener('mouseleave',disengage);