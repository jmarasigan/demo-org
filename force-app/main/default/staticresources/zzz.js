       var canvas, ctx, flag = false,
            prevX = 0,
            currX = 0,
            prevY = 0,
            currY = 0,
            dot_flag = false;
        
        
        var x = "black",
            y = 2;
        
        function init() {
            canvas = document.getElementById('can');
            ctx = canvas.getContext('2d');
            ctx.fillStyle = "white";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            w = canvas.width;
            h = canvas.height;
            
            canvas.addEventListener("mousemove", function (e) {
                findxy('move', e)
            }, false);
            canvas.addEventListener("mousedown", function (e) {
                findxy('down', e)
            }, false);
            canvas.addEventListener("mouseup", function (e) {
                findxy('up', e)
            }, false);
            canvas.addEventListener("mouseout", function (e) {
                findxy('out', e)
            }, false);
            
            canvas.addEventListener('touchstart', function (t) {
                findxytouch('down', t);
            },false);
            canvas.addEventListener('touchmove', function (t) {
                findxytouch('move', t);
            },false);
            canvas.addEventListener('touchend', function (t) {
                findxytouch('up', t);
            },false);
            canvas.addEventListener('touchout', function (t) {
                findxytouch('out', t);
            },false);
            
        }
        function draw() {
            ctx.beginPath();
            ctx.moveTo(prevX, prevY);
            ctx.lineTo(currX, currY);
            ctx.strokeStyle = x;
            ctx.lineWidth = y;
            ctx.stroke();
            ctx.closePath();
        }
        
        function findxytouch(touch, e) {
            if (touch == 'down') {
                prevX = currX;
                prevY = currY;
                currX = e.touches[0].clientX - canvas.getBoundingClientRect().left;
                currY = e.touches[0].clientY - canvas.getBoundingClientRect().top;
                
                flag = true;
                dot_flag = true;
                if (dot_flag) {
                    ctx.beginPath();
                    ctx.fillStyle = x;
                    ctx.fillRect(currX, currY, 2, 2);
                    ctx.closePath();
                    dot_flag = false;
                }
            }
            if (touch == 'up' || touch == "out") {
                flag = false;
            }
            if (touch == 'move') {
                if (flag) {
                    prevX = currX;
                    prevY = currY;
                    currX = e.touches[0].clientX - canvas.getBoundingClientRect().left;
                    currY = e.touches[0].clientY - canvas.getBoundingClientRect().top;
                    draw();
                }
            }
        }
        
        function findxy(res, e) {
            if (res == 'down') {
                prevX = currX;
                prevY = currY;
                currX = e.clientX - canvas.getBoundingClientRect().left;
                currY = e.clientY - canvas.getBoundingClientRect().top;
                
                flag = true;
                dot_flag = true;
                if (dot_flag) {
                    ctx.beginPath();
                    ctx.fillStyle = x;
                    ctx.fillRect(currX, currY, 2, 2);
                    ctx.closePath();
                    dot_flag = false;
                }
            }
            if (res == 'up' || res == "out") {
                flag = false;
            }
            if (res == 'move') {
                if (flag) {
                    prevX = currX;
                    prevY = currY;
                    currX = e.clientX - canvas.getBoundingClientRect().left;
                    currY = e.clientY - canvas.getBoundingClientRect().top;
                    draw();
                }
            }
        }
        
        init();