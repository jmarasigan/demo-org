/* Created by Mark Kevin Mapacpac */

//56aaff - ecebea - 706e6b
var canvas = document.getElementById("canvasBoard");
var content = document.getElementById("content");
var context = canvas.getContext("2d");

//START TRIAL
// console.log("trialx width: " + trialx.offsetWidth);
console.log("content: " + document.width + " : " + document.height);
console.log("canvas: " + canvas.width + " : " + canvas.height);

var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

console.log('lastry: ' + x + " : " + y);
//END-TRIAL


var dragging, dragStartLocation, snapshot, shape='line' ,color='black', savePoint;

function init(){
    
    context.strokeStyle = 'black';
    context.lineWidth = 5;
    context.lineCap = 'round';
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillStyle = "white";
    context.fillRect(0, 0, canvas.width, canvas.height);
}

function getCanvasCoordinates(event) {
    var x = event.clientX - canvas.getBoundingClientRect().left,
        y = event.clientY - canvas.getBoundingClientRect().top;

    return {x: x, y: y};
}



function drawLine(position) {
    
	context.fillStyle = color;
    context.lineTo(position.x, position.y);
    context.stroke();
    context.beginPath();
    context.arc(position.x, position.y, 2, 0, Math.PI*2);
    context.fill();
    context.beginPath();
    context.moveTo(position.x, position.y);
	
}

function drawCircle(position) {
	
	var ellCenter_x = dragStartLocation.x - (dragStartLocation.x - position.x)/2;
	var ellCenter_y = dragStartLocation.y - (dragStartLocation.y - position.y)/2;
	var ellWidth = Math.abs((dragStartLocation.x - position.x)/2);
	var ellHeight = Math.abs((dragStartLocation.y - position.y)/2);
	
    context.beginPath();
	context.ellipse(ellCenter_x, ellCenter_y, ellWidth , ellHeight,0 * Math.PI/180,0,2 * Math.PI);
    context.stroke();
}

function drawRectangle(position){
	
	var centerx = dragStartLocation.x - position.x; //distance between x
	var centery = dragStartLocation.y - position.y; //distance between y
	
	context.beginPath();
	context.rect(dragStartLocation.x,dragStartLocation.y, centerx*(-1),centery*(-1));
	context.stroke();
}

function drawEraser(position){
    
    context.fillStyle = 'white';
    context.strokeStyle = 'white';
    context.lineTo(position.x, position.y);
    context.stroke();
    context.beginPath();
    context.arc(position.x, position.y, 2, 0, Math.PI*2);
    context.fill();
    context.beginPath();
    context.moveTo(position.x, position.y);
    
}

function clearCanvas(){
    takeSavePoint();
    context.clearRect(0,0,canvas.width ,canvas.height);
    context.fillStyle = "white";
    context.fillRect(0, 0, canvas.width, canvas.height);
    
}

function takeSavePoint() {
	
    savePoint = context.getImageData(0, 0, canvas.width, canvas.height);
    
}

function restoreSavePoint() {
    if(savePoint != null || savePoint != undefined){
	   context.putImageData(savePoint, 0, 0);
    }
}

function takeSnapshot() {
	
    snapshot = context.getImageData(0, 0, canvas.width, canvas.height);
}

function restoreSnapshot() {
    context.putImageData(snapshot, 0, 0);
}


function engage(event){
    
    takeSavePoint();
    dragging = true;
	dragStartLocation = getCanvasCoordinates(event);
	if(shape == 'ellipse' || shape == 'rectangle'){
		takeSnapshot();
        context.beginPath();
	}else if (shape == 'line'){
        if(dragging === true){
            context.lineCap= "round";
            drawLine(getCanvasCoordinates(event));
        }
    }else if (shape == 'eraser'){
        if(dragging === true){
            
            drawEraser(getCanvasCoordinates(event));
        }
    }
    
}

function disengage(event) {
    
    dragging = false;
	
	var position = getCanvasCoordinates(event);
	if(shape=="rectangle" && dragging){
		restoreSnapshot();
		drawRectangle(position);
        context.beginPath();
        
	}else if(shape == "ellipse" && dragging){
		restoreSnapshot();
		drawCircle(position);
        context.beginPath();
        
	}else if(shape == "line" || shape == 'eraser'){
			context.beginPath();
    }
    //drawLine(position);
    context.beginPath();
}

function drag(event) {
    var position;
    if (dragging === true) {
		
        var position = getCanvasCoordinates(event);
		if(shape=="rectangle"){
			restoreSnapshot();
			drawRectangle(position);
		}else if(shape == "ellipse"){
			restoreSnapshot();
			drawCircle(position);
		}else if(shape == "line"){
            drawLine(position);
        }else if(shape == "eraser"){
            drawEraser(position);
        }
		//drawLine(position);
    }
}

function colorPanel(){
	
    var panel = document.createElement('div');
	panel.className = 'swatch';
	panel.style.background = "black";
	panel.addEventListener('click', setColor);
	document.getElementById('colors').appendChild(panel);
    
	var panel = document.createElement('div');
	panel.className = 'swatch';
	panel.style.background = "red";
	panel.addEventListener('click', setColor);
	document.getElementById('colors').appendChild(panel);
	
	
	var panel = document.createElement('div');
	panel.className = 'swatch';
	panel.style.background = "blue";
	panel.addEventListener('click', setColor);
	document.getElementById('colors').appendChild(panel);
}

function setColor(event){
    
	var panelClicked = event.target;
	color = panelClicked.style.backgroundColor;
	context.fillStyle = color;
    context.strokeStyle = color;
	
}

function tools(){
    
    var tool = document.createElement('BUTTON');
    tool.value = 'line';
    tool.className = 'tool button_active slds-button'
    //tool.style.width = "50px";
    tool.style.backgroundImage = "url('/resource/penIcon5')";
    tool.title = "Pen Tool";
    tool.style.backgroundPosition = "center";
    tool.style.backgroundSize = "cover";
    //tool.style.background = "white";
    tool.addEventListener('click', selectTool);
    document.getElementById('tools').appendChild(tool);
    
    var tool = document.createElement('BUTTON');
    tool.value = 'rectangle';
    tool.className = 'tool slds-button';
    //tool.style.width = "50px";
    tool.style.backgroundImage = "url('/resource/squareIcon4')";
    tool.title = "Rect Tool";
    tool.style.backgroundSize = "cover";
    tool.style.backgroundPosition = "center";
    //tool.innerHTML = "RECTANGLE";
    tool.addEventListener('click', selectTool);
    document.getElementById('tools').appendChild(tool);
    
    var tool = document.createElement('BUTTON');
    tool.value = 'ellipse';
    tool.className = 'tool slds-button';
    //tool.style.width = "50px";
    tool.style.backgroundImage = "url('/resource/circleIcon2')";
    tool.title = "Circle Tool";
    tool.style.backgroundPosition = "center";
    tool.style.backgroundSize = "cover";
    //tool.innerHTML = "ELLIPSE";
    tool.addEventListener('click', selectTool);
    document.getElementById('tools').appendChild(tool);
    
    var tool = document.createElement('BUTTON');
    tool.value = 'eraser';
    tool.className = 'tool slds-button';
    tool.title = "Eraser Tool";
    //tool.style.width = "50px";
    tool.style.backgroundImage = "url('/resource/eraserIcon2')";
    tool.style.backgroundPosition = "center";
    tool.style.backgroundSize = "cover";
    //tool.innerHTML = "ERASER";
    tool.addEventListener('click', selectTool);
    document.getElementById('tools').appendChild(tool);
    
    var tool = document.createElement('BUTTON');
    tool.value = 'undo';
    tool.className = 'tool slds-button';
    tool.title = "Undo Tool";
    tool.style.backgroundImage = "url('/resource/undoIcon2')";
    tool.style.backgroundPosition = "center";
    tool.style.backgroundSize = "cover";
    //tool.style.background = "white";
    //tool.innerHTML = "UNDO";
    //console.log("tool" + tool);
    tool.addEventListener('click', restoreSavePoint);
    document.getElementById('tools').appendChild(tool);
    
    
    var tool = document.createElement('BUTTON');
    tool.value = 'clear';
    tool.className = 'tool slds-button';
    tool.title = "Clear Tool";
    tool.style.backgroundImage = "url('/resource/clearIcon2')";
    tool.style.backgroundPosition = "center";
    tool.style.backgroundSize = "cover";
    //tool.style.background = "white";
    //tool.innerHTML = "CLEAR";
    //console.log("tool" + tool);
    tool.addEventListener('click', clearCanvas);
    document.getElementById('tools').appendChild(tool);
    
}

function selectTool(event){
    
    context.fillStyle = color;
    context.strokeStyle = color;
    var toolClicked = event.target;
    var toolChsn = toolClicked.value;
    shape = toolChsn;
    var active = document.getElementsByClassName('button_active')[0];
    if(active){
       active.className = 'tool slds-button';
        //active.style.backgroundColor = 'white';
        //active.style.border = 'none';
    }
    //toolClicked.style.backgroundColor = '#666';
    //toolClicked.style.border = '3px solid blue';
    toolClicked.className += ' button_active';
    
    
}

tools();
colorPanel();
init();

canvas.addEventListener('touchstart', engage);
canvas.addEventListener('touchmove', drag);
canvas.addEventListener('touchend', disengage);


canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mousemove', drag);
canvas.addEventListener('mouseup', disengage);
canvas.addEventListener('mouseleave',disengage);
